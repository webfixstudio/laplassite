<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class Article extends Model {

	/*
	 * Используется для защиты полей список полей доступных для пользовател.
	 */
	protected $fillable = ['title', 'body', 'published_at'];

	/*
	 * мутатор даты использует карбон для создания обьекта времени.
	 */
	protected $dates = ['published_at'];

	public function scopePublished($query)
	{
		$query->where('published_at', '<=', Carbon::now());
	}

	public function scopeUnpublished($query)
	{
		$query->where('published_at', '>', Carbon::now());
	}

	public function scopeInfo($query)
	{
		$query->where('published_at', '>', Carbon::now());
	}

	/**
	 * @param $date
	 */
	public function setPublishedAtAttribute($date)
	{
		$this->attributes['published_at'] = Carbon::parse($date);
	}

	public function getBodyAttribute($value)
    {
        return ucfirst($value);
    }


	/**
	 * An article is owned ba a user.
	 *
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function tags()
	{
		return $this->belongsToMany('App\Tag')->withTimestamps();
	}

	public function getTagListAttribute()
	{
		return $this->tags->lists('id');
	}

	public function category()
	{
		return $this->belongsToMany('App\Categories')->withTimestamps();
	}

	public function getCategoryListAttribute()
	{
		return $this->category->lists('id');
	}
}
