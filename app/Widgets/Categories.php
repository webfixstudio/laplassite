<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;


class Categories extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */

    protected $config = [
        'count' => 5,

    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $categories = \App\Categories::all();
        return view("widgets.categories", [
            'config' => $this->config,
            'categories' => $categories,

        ]);
    }
}
