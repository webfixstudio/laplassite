<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;


class Gallery extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */

    protected $config = [
        'count' => 5,

    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $images = \App\Image::all();
        return view("widgets.gallery", [
            'config' => $this->config,
            'images' => $images,

        ]);
    }
}
