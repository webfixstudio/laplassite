<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model {

    protected $fillable = [
        'category_name',
        'parent_id',
        'category_alias',
        'description',
        'category_image',
        'meta_keywords',
        'meta_description',
        'meta_title'
    ];

    public function articles()
    {
        return $this->belongsToMany('App\Article')->withTimestamps();
    }

}
