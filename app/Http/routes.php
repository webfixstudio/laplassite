<?php

$server = League\Glide\ServerFactory::create([
		'source' => 'user_panel/img',
		'cache' => 'user_panel/img/.cache',
]);

	Route::controllers(['auth' => 'Auth\AuthController','password' => 'Auth\PasswordController']);

	Route::get('fff', function(){abort(404,'my mess',['sdfsdf']);});
	Route::get('upl', 'ArticlesController@upl');
	Route::post('apply/multiple_upload', 'ArticlesController@multiple_upload');
 
/* ----- Pages ---------------------------------------------------- */
	Route::get  ('/', 'PagesController@index')													->name('home');
	Route::get  ('about','PagesController@about')												->name('about');
	Route::get  ('contact', 'PagesController@contact')									->name('contact');
	Route::post ('contact','PagesController@contact_send')							->name('contact.send');

/* ----- Articles ------------------------------------------------- */
	Route::get 	  ('articles','ArticlesController@index')								->name('articles');
	Route::post   ('articles','ArticlesController@store')								->name('article.store');
	Route::get    ('article/create','ArticlesController@create')				->name('article.create');
	Route::get    ('article/{id}','ArticlesController@show')						->name('article.show');
	Route::put    ('article/{id}','ArticlesController@update')					->name('article.update');
	Route::delete ('article/{id}','ArticlesController@destroy')					->name('article.destroy');
	Route::get    ('article/{id}/edit','ArticlesController@edit')				->name('article.edit');
	Route::get    ('category/{id}','ArticlesController@category')				->name('article.category');
	Route::get    ('tag/{id}','ArticlesController@tag')									->name('article.tag');
	Route::post   ('search','ArticlesController@search')								->name('article.search');

/* ----- Images --------------------------------------------------- */
	Route::get 	  ('images','ImageController@index')										->name('images');
	Route::post   ('images','ImageController@store')										->name('image.store');
	Route::get    ('image/create','ImageController@create')							->name('image.create');
	Route::get    ('image/{id}','ImageController@show')									->name('image.show');
	Route::put    ('image/{id}','ImageController@update')								->name('image.update');
	Route::delete ('image/{id}','ImageController@destroy')							->name('image.destroy');
	Route::get    ('image/{id}/edit','ImageController@edit')						->name('image.edit');

/* ----- Users ---------------------------------------------------- */
	// Route::get    ('users','UserController@index')											->name('users');
	Route::post    ('users','UserController@update')											->name('user.update');
	Route::get    ('user/{id}','UserController@show')										->name('user.show');
	Route::get    ('user/{id}/edit','UserController@edit')							->name('user.edit');

/* --- ADMIN --- ADMIN --- ADMIN --- ADMIN --- ADMIN --- ADMIN --- */

	Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {

	/* ----- Panel -------------------------------------------------- */
	Route::get  ('/','AdminController@index')														->name('admin');

	/* ----- Categories --------------------------------------------- */
	Route::get  ('categories','Admin\CategoriesController@index')					->name('admin_categories');
	Route::post ('categories','Admin\CategoriesController@store')					->name('admin_category.store');
	Route::get  ('category/create','Admin\CategoriesController@create')		->name('admin_category.create');
	Route::get  ('category/{id}','Admin\CategoriesController@show')				->name('admin_category.show');
	Route::post ('category/{id}','Admin\CategoriesController@update')			->name('admin_category.update');
	Route::get  ('category/{id}/del','Admin\CategoriesController@destroy')->name('admin_category.destroy');
	Route::get  ('category/{id}/edit','Admin\CategoriesController@edit')	->name('admin_category.edit');

	/* ----- Users -------------------------------------------------- */
	Route::get  ('users','Admin\UsersController@index')									->name('admin_users');
	Route::get  ('user/{id}','Admin\UsersController@show')							->name('admin_user.show');
	Route::post ('user/{id}','Admin\UsersController@update')						->name('admin_user.update');
	Route::get  ('user/{id}/del','Admin\UsersController@destroy')				->name('admin_user.destroy');
	Route::get  ('user/{id}/edit','Admin\UsersController@edit')					->name('admin_user.edit');

	/* ----- Articles ----------------------------------------------- */
	Route::get  ('articles','Admin\ArticlesController@index')						->name('admin_articles');
	Route::post ('articles','Admin\ArticlesController@store')						->name('admin_article.store');
	Route::get  ('article/create','Admin\ArticlesController@create')		->name('admin_article.create');
	Route::get  ('article/{id}','Admin\ArticlesController@show')				->name('admin_article.show');
	Route::post ('article/{id}','Admin\ArticlesController@update')			->name('admin_article.update');
	Route::get 	('article/{id}/del','Admin\ArticlesController@destroy')	->name('admin_article.destroy');
	Route::get  ('article/{id}/edit','Admin\ArticlesController@edit')		->name('admin_article.edit');

	/* ----- Tags --------------------------------------------------- */
	Route::get  ('tags','Admin\TagsController@index')										->name('admin_tags');
	Route::post ('tags','Admin\TagsController@store')										->name('admin_tag.store');
	Route::get  ('tag/create','Admin\TagsController@create')						->name('admin_tag.create');
	Route::get  ('tag/{id}','Admin\TagsController@show')								->name('admin_tag.show');
	Route::post ('tag/{id}','Admin\TagsController@update')							->name('admin_tag.update');
	Route::get  ('tag/{id}/del','Admin\TagsController@tagdelete')				->name('admin_tag.destroy');
	Route::get  ('tag/{id}/edit','Admin\TagsController@edit')						->name('admin_tag.edit');

	/* ----- Images ------------------------------------------------- */
	Route::get  ('images','Admin\ImagesController@index')								->name('admin_images');
	Route::post ('images','Admin\ImagesController@store')								->name('admin_image.store');
	Route::get  ('image/create','Admin\ImagesController@create')				->name('admin_image.create');
	Route::get  ('image/{id}','Admin\ImagesController@show')						->name('admin_image.show');
	Route::post ('image/{id}','Admin\ImagesController@update')					->name('admin_image.update');
	Route::get  ('image/{id}/del','Admin\ImagesController@destroy')			->name('admin_image.destroy');  
	Route::get  ('image/{id}/edit','Admin\ImagesController@edit')				->name('admin_image.edit');
});