<?php 
namespace App\Http\Controllers;

// use Validator;
// use App\Http\Requests;
// use App\Image;
// use App\Tag;
// use Illuminate\Http\Request;
use App\User;
use App\Article;
use App\Http\Controllers\Controller;

class AdminController extends Controller {

	/**
	 * блокирует все действия со статьями.
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		$users= \DB::table('users')->get();
		$tags= \DB::table('tags')->get();
		$articles= \DB::table('articles')->latest()->limit(10)->get();
		return view('admin.dashboard.index',compact('users', 'articles', 'tags'));
	}

/* COMMON ADMIN	PANEL ------------------------------------------------------- */

/*	public function create()
	{
		//
	}

	public function store()
	{
		//
	}

	public function show($id)
	{
		//
	}

	public function edit($id)
	{
		//
	}

	public function update($id)
	{
		//
	}

	public function destroy($id)
	{
		//
	}




/* USERS ADMIN PANEL ------------------------------------------------------- *
	public function users()
	{
		$allusers = User::all();
		return view('admin.users.index', compact('allusers'));
	}

	public function userview($id)
	{
		$user = User::find($id);
		return view('admin.users.view', compact('user'));
	}

	public function useradd()
	{
		return view('admin.users.add');
	}

	public function useredit($id)
	{
		$user = User::find($id);
		return view('admin.users.edit', compact('user'));
	}

	public function usersave()
	{
		$user = \Input::all();
		User::create($user);
		return redirect('admin/tags/index');
	}

	public function userupdate(User $user)
	{

	}

	public function userdelete($id)
	{
		User::destroy($id);
		return redirect('admin/users/index');
	}


	/* ARTICLES ADMIN PANEL ------------------------------------------------------- *
	public function articles()
	{
		$allarticles = Article::all();
		return view('admin.articles.index', compact('allarticles'));
	}

	public function articleview($id)
	{
		$user = User::find($id);
		return view('admin.articles.view', compact('user'));
	}

	public function articleadd()
	{
		$tags = \App\Tag::lists('name', 'id');
		$category = \App\Categories::lists('category_name', 'id');
		return view('admin.articles.add', compact('tags','category'));
	}

	public function articleedit($id)
	{
		$user = Article::find($id);
		return view('admin.articles.edit', compact('user'));
	}

	public function articlesave()
	{
		$user = \Input::all();
		Article::create($user);
		return redirect('admin/articles/index');
	}

	public function articleupdate(User $user)
	{

	}

	public function articledelete($id)
	{
		Article::destroy($id);
		return redirect('admin/articles/index');
	}


	/* TAGS ADMIN PANEL ------------------------------------------------------- *
	public function tags()
	{
		$tags = Tag::all();
		return view('admin.tags.index', compact('tags'));
	}

	public function tagview($id)
	{
		$tagdeas = Tag::find($id)->get();
		$tags = \DB::table('tags')
			->leftJoin('article_tag', 'tags.id', '=', 'article_tag.tag_id')
			->leftJoin('articles', 'article_tag.article_id', '=', 'articles.id')
			->where('tag_id','=', $id)
			->get();
		return view('admin.tags.view', compact('tags', 'tagdeas'));

	}

	public function tagadd()
	{
		return view('admin.tags.add');
	}

	public function tagedit($id)
	{
		$tag = Tag::find($id);
		return view('admin.tags.edit',compact('tag'));
	}

	public function tagsave()
	{
		$tag = \Input::all();
		$validator = Validator::make($tag, ['name' => 'required|min:5']);
		if ($validator->fails())
		{
			
			return redirect('/admin/tags/add')->withErrors($validator);
		}else{
			\Session::flash('error', 'uploaded file is not valid');
			Tag::create($tag);
			return redirect('/admin/tags/index');
		}
	}

	public function tagupdate(Tag $tag)
	{
		$taginput = \Input::get('name');
		$tag->update($taginput);
		return redirect('/admin/tags/index');
	}

	public function tagdelete($id)
	{
		Tag::destroy($id);
		return redirect('admin/tags/index');
	}

	/* IMAGES ADMIN PANEL ------------------------------------------------------- *
	public function images()
	{
		$images = Image::paginate(10);
		return view('admin.images.index', compact('images'));
	}

	public function imageview($id)
	{
		$image = Image::find($id);
		return view('admin.images.view', compact('image'));

	}

	public function imageadd()
	{
		return view('admin.images.add');
	}

	public function imageedit($id)
	{
		$image = Image::find($id);
		return view('admin.images.edit',compact('image'));
	}

	public function imagesave(Request $request)
	{
		// Validation //
		$validation = Validator::make($request->all(), [
			'caption'     => 'required|regex:/^[A-Za-z ]+$/',
			'description' => 'required',
			'userfile'     => 'required|image|mimes:jpeg,png|min:1|max:250'
		]);

		// Check if it fails //
		if( $validation->fails() ){
			return redirect()->back()->withInput()
				->with('errors', $validation->errors() );
		}

		$image = new Image;

		// upload the image //
		$file = $request->file('userfile');
		$destination_path = 'uploads/';
		$filename = str_random(6).'_'.$file->getClientOriginalName();
		$file->move($destination_path, $filename);

		// save image data into database //
		$image->file = $destination_path . $filename;
		$image->caption = $request->input('caption');
		$image->description = $request->input('description');
		$image->save();

		return redirect('/admin/images/index')->with('message','You just uploaded an image!');
	}

	public function imageupdate(Request $request, $id)
	{
// Validation //
		$validation = Validator::make($request->all(), [
			'caption'     => 'required|regex:/^[A-Za-z ]+$/',
			'description' => 'required',
			'userfile'    => 'sometimes|image|mimes:jpeg,png|min:1|max:250'
		]);

		// Check if it fails //
		if( $validation->fails() ){
			return redirect()->back()->withInput()
				->with('errors', $validation->errors() );
		}

		// Process valid data & go to success page //
		$image = Image::find($id);

		// if user choose a file, replace the old one //
		if( $request->hasFile('userfile') ){
			$file = $request->file('userfile');
			$destination_path = 'uploads/';
			$filename = str_random(6).'_'.$file->getClientOriginalName();
			$file->move($destination_path, $filename);
			$image->file = $destination_path . $filename;
		}

		// replace old data with new data from the submitted form //
		$image->caption = $request->input('caption');
		$image->description = $request->input('description');
		$image->save();

		return redirect('/admin/images/index')->with('message','You just updated an image!');
	}

	public function imagedelete($id)
	{
		Image::destroy($id);
		return redirect('admin/images/index');
	}
*/
}
