<?php namespace App\Http\Controllers\Admin;

use App\Article;
use App\Http\Requests;
use App\Http\Controllers\Controller;
// use App\Image;
// use App\Tag;
use Illuminate\Http\Request;
use App\User;
use Validator;

class UsersController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		$allusers = User::all();
		return view('admin.users.index', compact('allusers'));
	}

	public function show($id)
	{
		$user = User::find($id);
		return view('admin.users.show', compact('user'));
	}

	public function edit($id)
	{
		$user = User::find($id);
		return view('admin.users.edit', compact('user'));
	}
	
	public function update(User $user)
	{

	}

	public function destroy($id)
	{
		User::destroy($id);
		// return redirect('admin/users');
		return redirect()->route('ausers');
	}


	// public function usersave()
	// {
	// 	$user = \Input::all();
	// 	User::create($user);
	// 	return redirect('admin/tags/index');
	// }

}