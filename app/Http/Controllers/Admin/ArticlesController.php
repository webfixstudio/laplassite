<?php namespace App\Http\Controllers\Admin;

use Validator;
use App\Article;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use App\Image;
// use App\Tag;
// use App\User;

class ArticlesController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		$articles = Article::all();
		return view('admin.articles.index', compact('articles'));
	}

	public function show($id)
	{
		$article = Article::findOrFail($id);
		return view('admin.articles.show', compact('article'));
	}

	public function create()
	{
		// $tags = \App\Tag::lists('name', 'id');
		$tags = \App\Tag::lists('name');
		$category = \App\Categories::lists('category_name', 'id');
		// $category = \App\Categories::lists('category_name');
		return view('admin.articles.create', compact('tags','category'));
	}

	public function save(Request $request)
	{
		Article::create($request->all());
		$tagIds = $request->input('tag_list');
		$article->tags()->attach($tagIds);
		return redirect()->route('articles');
	}

	public function store(Request $request )
	{
		$article = Auth::user()->articles()->create($request->all());
		$tagIds = $request->input('tag_list');
		$article->tags()->attach($tagIds);
		$categoryIds = $request->input('category_list');
		$article->category()->attach($categoryIds);
		\Session::flash('flash_message','Your article has been created!');
		return redirect('articles');
	}

	public function edit($id)
	{
		$article = Article::findOrFail($id);
		return view('admin.articles.edit', compact('article'));
	}

	public function update(Request $request, $id)
	{

	}

	public function destroy($id)
	{
		Article::destroy($id);
		return redirect()->route('articles');
	}

}