<?php namespace App\Http\Controllers\Admin;

use Input;
use Validator;
use Redirect;
use Session;
use Image;

use App\Categories;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Auth;


class CategoriesController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		$categories = Categories::latest('created_at')->paginate(10);
		return view('admin.categories.index', compact('categories'));
	}

	public function create()
	{
		$categories = Categories::all();
		return view('admin.categories.create', compact('categories'));
	}

	public function store(Request $request)
	{
		$validation = Validator::make($request->all(), [
			//'caption'     => 'required|regex:/^[A-Za-z ]+$/',
			//'description' => 'required',
			//'category_image'     => 'required|image|mimes:jpeg,jpg,png|min:1|max:1024'
		]);

		if ($validation->fails()) {
			return redirect()->back()->withInput()->with('errors', $validation->errors());
		}

		$category = new Categories($request->all());
		if ($request->hasFile('category_image')) {
			$image = $request->file('category_image');
			$destination_path = '/files/category_images/';
			$filename = str_random(6) . '_' . $image->getClientOriginalName();
			$image->move($destination_path, $filename);
			$category->category_image = $filename;
		}else{
			$category->category_image = 'default.jpg';
		}
		$category->save();

		return redirect('admin/categories')->with('message', 'You just create category!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show($id)
	{
		$category = Categories::findOrFail($id);
		return view('admin.categories.show', compact('category'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category = Categories::findOrFail($id);
		return view('admin.categories.edit', compact('category'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		dd($id);
		$destination_path = '/files/category_images/';
		if ($request->hasFile('category_image')) {
			$image = $request->file('category_image');
			$filename = str_random(6) . '_' . $image->getClientOriginalName();
			dd($category->category_image);
			if (file_exists(public_path($destination_path . $category->category_image)) && $category->category_image != 'default.jpg') {
				unlink('files/users_images/' . $category->category_image);
			}
			$image->move($destination_path, $filename);
			$category->category_image = $filename;
		}
		if (Input::file('category_image')) {
			$image = Input::file('category_image');
			$filename = str_random(6) . '_' . $image->getClientOriginalName();

			$path = public_path('/files/category_images/' . $filename);
			Image::make($image->getRealPath())
				->resize(null, 405, function ($constraint) {
					$constraint->aspectRatio();
					//$constraint->upsize();
				})
				->crop(400, 400)
				->insert('watermark.png', 'bottom-right', 20, 0)
				->save($path);
			$category->category_image = $filename;
			$category->save();
		}

		var_dump($request->all());
		// $category->update($request->all());
		// return redirect('admin/categories');
	}

	public function destroy($id)
	{
		Categories::destroy($id);
		return redirect()->route('categories');
	}

}
