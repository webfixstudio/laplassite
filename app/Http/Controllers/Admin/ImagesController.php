<?php namespace App\Http\Controllers\Admin;

use App\Article;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Image;
use Illuminate\Http\Request;
use Validator;

class ImagesController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		$images = Image::paginate(10);
		return view('admin.images.index', compact('images'));
	}

	public function show($id)
	{
		$image = Image::findOrFail($id);
		return view('admin.images.show', compact('image'));
	}

	public function create()
	{
		return view('admin.images.create');
	}

	public function store(Request $request)
	{
		// Validation //
		$validation = Validator::make($request->all(), [
			'caption'     => 'required|regex:/^[A-Za-z ]+$/',
			'description' => 'required',
			'image'     => 'required|image|mimes:jpeg,png|min:1|max:1024'
		]);

		// Check if it fails //
		if( $validation->fails() ){
			return redirect()->back()->withInput()
				->with('errors', $validation->errors() );
		}

		$image = new Image;

		// upload the image //
		$file = $request->file('image');
		$destination_path = 'uploads/';
		$filename = str_random(6).'_'.$file->getClientOriginalName();
		$file->move($destination_path, $filename);

		// save image data into database //
		$image->file = $destination_path . $filename;
		$image->caption = $request->input('caption');
		$image->description = $request->input('description');
		$image->save();

		// return redirect()->route('image.index')->with('message','You just uploaded an image!');
		return redirect()->route('images');

	}

	public function edit($id)
	{
		$image = Image::find($id);
		return view('admin.images.edit',compact('image'));
	}

	public function update(Request $request, $id)
	{
// Validation //
		$validation = Validator::make($request->all(), [
			'caption'     => 'required|regex:/^[A-Za-z ]+$/',
			'description' => 'required',
			'image'    => 'sometimes|image|mimes:jpeg,png|min:1|max:250'
		]);

		// Check if it fails //
		if( $validation->fails() ){
			return redirect()->back()->withInput()
				->with('errors', $validation->errors() );
		}

		// Process valid data & go to success page //
		$image = Image::find($id);

		// if user choose a file, replace the old one //
		if( $request->hasFile('image') ){
			$file = $request->file('image');
			$destination_path = 'uploads/';
			$filename = str_random(6).'_'.$file->getClientOriginalName();
			$file->move($destination_path, $filename);
			$image->file = $destination_path . $filename;
		}

		// replace old data with new data from the submitted form //
		$image->caption = $request->input('caption');
		$image->description = $request->input('description');
		$image->save();
		return redirect()->route('images');
	}

	public function destroy($id)
	{
		Image::destroy($id);
		return redirect()->route('images');
	}

}