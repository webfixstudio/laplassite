<?php namespace App\Http\Controllers\Admin;

use App\Article;
use App\Http\Requests;
use App\Http\Controllers\Controller;
// use App\Image;
use App\Tag;
use Illuminate\Http\Request;
// use App\User;
use Validator;

class TagsController extends Controller {


	public function __construct()
	{
		$this->middleware('auth');
	}
/* TAGS ADMIN PANEL ------------------------------------------------------- */
	public function index()
	{
		$tags = Tag::all();
		return view('admin.tags.index', compact('tags'));
	}

	public function show($id)
	{
		$tag = Tag::findOrFail($id);
		$tags = \DB::table('tags')
			->leftJoin('article_tag', 'tags.id', '=', 'article_tag.tag_id')
			->leftJoin('articles', 'article_tag.article_id', '=', 'articles.id')
			->where('tag_id','=', $id)
			->get();
		return view('admin.tags.show', compact('tags', 'tag'));

	}

	public function create()
	{
		return view('admin.tags.add');
	}

	public function edit($id)
	{
		$tag = Tag::find($id);
		return view('admin.tags.edit',compact('tag'));
	}

	public function store()
	{
		$tag = \Input::all();
		$validator = Validator::make($tag, ['name' => 'required|min:5']);
		if ($validator->fails())
		{
			/*\Session::flash('error', 'uploaded file is not valid');
			$validator->errors()->add('field', 'Something is wrong with this field!');*/
			return redirect('/admin/tags/add')->withErrors($validator);
		}else{
			\Session::flash('error', 'uploaded file is not valid');
			Tag::create($tag);
			return redirect('/admin/tags/index');
		}
	}

	public function update(Tag $tag)
	{
		$taginput = \Input::get('name');
		$tag->update($taginput);
		return redirect('/admin/tags/index');
	}

	public function destroy($id)
	{
		Tag::destroy($id);
		return redirect('admin/tags/index');
	}
}