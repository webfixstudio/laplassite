<?php 
namespace App\Http\Controllers;

/* -- UPLOAD -- */
// use Input;
// use Validator;
// use Redirect;
// use Session;
/* -- UPLOAD -- */

use App\Tag;
use App\Article;
use App\Categories;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
// use App\Http\Requests;
// use Illuminate\Support\Facades\DB;;
// use Illuminate\Support\Facades\Auth;
// use phpDocumentor\Reflection\DocBlock\Tag\ReturnTag;

class ArticlesController extends Controller {

	public function __construct()
	{
		$this->middleware('auth', ['only' => ['create','edit']]);
	}

	public function index()
	{
		$articles = Article::latest('published_at')->published()->paginate(10);
			return view('articles.index', compact('articles','terms'));
	}

	public function show($id)
	{
		// \Session::flash('message_my', "hello me");
		$article = Article::findOrFail($id);
		return view('articles.show', compact('article'));
	}

	public function create()
	{
		$tags = \App\Tag::lists('name', 'id');
		$category = \App\Categories::lists('category_name', 'id');
		return view('articles.create', compact('tags','category'));
	}

	public function store(ArticleRequest $request )
	{
		$article = Auth::user()->articles()->create($request->all());
		$tagIds = $request->input('tag_list');
		$article->tags()->attach($tagIds);
		$categoryIds = $request->input('category_list');
		$article->category()->attach($categoryIds);
		\Session::flash('flash_message','Your article has been created!');
		return redirect('articles');
	}

	public function edit($id)
	{
		$article = Article::findOrFail($id);
		$tags = Tag::lists('name', 'id');
		$category = Categories::lists('category_name', 'id');
		return view('articles.edit',compact('article','tags','category'));
	}

	public function update(ArticleRequest $request, $id)
	{
		$article = Article::findOrFail($id);
		$article->update($request->all());
		$tagIds = $request->input('tag_list');
		$article->tags()->sync($tagIds);
		$categoryIds = $request->input('category_list');
		$article->category()->sync($categoryIds);
				\Session::flash('flash_message','Your article has been updated!');
		return redirect('articles');
		// return "ffff";
	}

	public function destroy($id)
	{
		Article::findOrFail($id)->delete();
		return redirect('articles');
	}

	public function tag($id)
	{
		$tag = Tag::findOrFail($id);
		return view('articles.tag', compact('tag'));
	}

	public function category($id)
	{
		$category = Categories::findOrFail($id);
		return view('articles.category', compact('category'));
	}

	public function search(Request $request)
	{
		$search = $request->search;
		if ($search !== ''){
		$articles = Article::latest('published_at')
			->where('body', 'like', '%'.$request->search.'%')
			->orWhere('title', 'like', '%'.$request->search.'%')
			->published()
			->paginate(10);
		return view('articles.search', compact('articles','search'));
		}else{
			session()->flash('status', 'Task was successful!');
			return redirect('articles');
			// return redirect('articles')->with('ddd','ddd ddd !!!!');
		}
	}














	public function multiple_upload() {
		// getting all of the post data
		$files = Input::file('images');
		// Making counting of uploaded images
		$file_count = count($files);
		// start count how many uploaded
		$uploadcount = 0;
		foreach($files as $file) {
			$rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			$validator = Validator::make(array('file'=> $file), $rules);
			if($validator->passes()){
				$destinationPath = '123';
				$filename = $file->getClientOriginalName();
				$upload_success = $file->move($destinationPath, $filename);
				$uploadcount ++;
			}
		}
		if($uploadcount == $file_count){
			Session::flash('success', 'Upload successfully');
			return Redirect::to('upload');
		}
		else {
			return Redirect::to('upload')->withInput()->withErrors($validator);
		}
	}

	public function upl()
	{
		return view('upload');
	}


}

