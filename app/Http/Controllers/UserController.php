<?php
namespace App\Http\Controllers;

use Input;
use Validator;
use Redirect;
use Session;

use App\User;
use Illuminate\Http\Request;
use Image;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
	public function __construct()
  {
		$this->middleware('auth');
  }

	public function index()
	{
		$users = \DB::table('users')->latest()->paginate(10);
		return view('user.all', compact('users'));
	}

	public function show($id)
	{
		$user = User::find($id);
		return view('user.index', compact('user'));
	}

	public function edit($id)
	{
		$user = User::find($id);
		return view('user.edit', compact('user'));
	}

	public function update($id)
	{
		$user = User::find(\Auth::user()->id);
		$request = Input::except('_method', '_token', 'foto');
		$origfoto = \Auth::user()->foto;
		if (Input::file('foto')) {
			$file = Input::file('foto');
			$fileName = time() . "-" . $file->getClientOriginalName();
			$path = public_path('files/users_images/' . $fileName);
			if (file_exists(public_path('files/users_images/' . $origfoto)) && $origfoto != 'default.jpg') {
				unlink('files/users_images/' . $origfoto);
			}
			Image::make($file->getRealPath())
				->resize(null, 405, function ($constraint){$constraint->aspectRatio();})
				->crop(400, 400)
				->save($path);
			$user->foto = $fileName;
			$user->save();
		}
		$user->update($request);
		return redirect()->route('user.show',$id);
		// Redirect::to('user/' . \Auth::user()->id);
	}

	public function upload()
	{
		// getting all of the post data
		$file = array('image' => Input::file('image'));
		// setting up rules
		$rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
		// doing the validation, passia ng post data, rules and the messages
		$validator = Validator::make($file, $rules);
		if ($validator->fails()) {
			// send back to the page with the input data and errors
			return Redirect::to('upload')->withInput()->withErrors($validator);
		} else {
			// checking file is valid.
			if (Input::file('image')->isValid()) {
				$file = Input::file('image');
				$fileName = time() . "-" . $file->getClientOriginalName();
				$path = public_path('files/users_images/' . $fileName);
				Image::make($file->getRealPath())
					->resize(null, 405, function ($constraint) {
						$constraint->aspectRatio();
						//$constraint->upsize();
					})
					->crop(400, 400)
					->save($path);
				// sending back with message
				Session::flash('success', 'Upload successfully');
				$user = User::find(\Auth::user()->id);
				//delete old foto
				if ((\Auth::user()->foto != 'default.jpg') and (file_exists(public_path('files/users_images/' . \Auth::user()->foto)))) {
					unlink('files/users_images/' . \Auth::user()->foto);
				}
				$user->foto = $fileName;
				$user->save();
				return Redirect::to('user/' . \Auth::user()->id);
			} else {
				// sending back with error message.
				Session::flash('error', 'uploaded file is not valid');
				return Redirect::to('upload');
			}
		}
	}
}
