<?php 
namespace App\Http\Controllers;

use App\Image;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ImageController extends Controller {

	public function __construct()
  {
    $this->middleware('auth', ['only' => ['create','edit']]);
  }

	public function index()
	{
		$images = Image::paginate(8);
		return view('images.index')->with('images', $images);
	}


	public function show($id)
	{
		$image = Image::find($id);
		return view('images.show')->with('image', $image);
	}

	public function create()
	{
		// return "dddd";
		return view('images.create');
	}


	public function store(Request $request)
	{
		// Validation //
		$validation = Validator::make($request->all(), [
			'caption'     => 'required|regex:/^[A-Za-z ]+$/',
			'description' => 'required',
			'userfile'     => 'required|image|mimes:jpeg,png|min:1|max:1024'
		]);

		// Check if it fails //
		if( $validation->fails() ){
			return redirect()->back()->withInput()
				->with('errors', $validation->errors() );
		}

		$image = new Image;

		// upload the image //
		$file = $request->file('userfile');
		$destination_path = 'uploads/';
		$filename = str_random(6).'_'.$file->getClientOriginalName();
		$file->move($destination_path, $filename);

		// save image data into database //
		$image->file = $destination_path . $filename;
		$image->caption = $request->input('caption');
		$image->description = $request->input('description');
		$image->save();

		return redirect()->route('image.index')->with('message','You just uploaded an image!');
	}


	public function edit($id)
	{
		$image = Image::find($id);
		return view('images.edit')->with('image', $image);
	}

	public function update(Request $request, $id)
	{
		$validation = Validator::make($request->all(), [
			'caption'     => 'required|regex:/^[A-Za-z ]+$/',
			'description' => 'required',
			'userfile'    => 'sometimes|image|mimes:jpeg,png|min:1|max:1024'
		]);
		if( $validation->fails() ){
			return redirect()->back()->withInput()
				->with('errors', $validation->errors() );
		}

		// Process valid data & go to success page //
		$image = Image::find($id);

		// if user choose a file, replace the old one //
		if( $request->hasFile('userfile') ){
			unlink($image->file);
			$file = $request->file('userfile');
			$destination_path = 'uploads/';
			$filename = str_random(6).'_'.$file->getClientOriginalName();
			$file->move($destination_path, $filename);
			$image->file = $destination_path . $filename;
		}

		// replace old data with new data from the submitted form //
		$image->caption = $request->input('caption');
		$image->tag = $request->tag	;
		$image->description = $request->input('description');
		$image->save();

		return redirect()->route('image.index')->with('message','You just updated an image!');
	}

	public function destroy($id)
	{
		$image = Image::find($id);
		unlink($image->file);
		$image->delete();
		return redirect()->route('images')->with('message','You just uploaded an image!');
	}

}
