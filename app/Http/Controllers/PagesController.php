<?php 
namespace App\Http\Controllers;

use App\Article;
use App\Image;
use Input;
use Validator;
use Redirect;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;

class PagesController extends Controller
{

	public function index()
	{
		// $articles = Article::published()->orderBy('published_at', 'desc')->take(5)->get();
		// $images = Image::all();
		return view('welcome');
	}

	public function about()
	{
		return view('pages.about');
	}

	public function contact()
	{
		return view('pages.contact');
	}

	//Contact Form
	public function contact_send()
	{
		$data = Input::all();

		$rules = array(
			'first_name' => 'required|alpha',
			'last_name' => 'required|alpha',
			'phone_number' => 'numeric|min:8',
			'email' => 'required|email',
			'message' => 'required|min:25'
		);
		$validator = Validator::make($data, $rules);

		if ($validator->passes()) {
			Mail::send('emails.hello', $data, function ($message) use ($data) {
				$message->from($data['email'], $data['first_name']);
				$message->to('webfix@ukr.net', 'Name Nicolas')->cc('webfix@ukr.net')->subject('contact request');
			});
			return view('pages.contact');
		} else {
			return Redirect::to('/contact')->withErrors($validator);
		};
	}

}
