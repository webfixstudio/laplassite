<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

	protected $fillable = ['name'];
	/**
	 *Get the articles associates with the given tag.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function article()
	{
		return $this->belongsToMany('App\Article');
	}

	public function scopeCreate1(){

	}
}
