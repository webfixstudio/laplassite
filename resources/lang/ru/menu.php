<?php

return [

	"home" => "Главная",
	"articles" => "Статьи",
	"images" => "Фотографии",
	"contact" => "Контакты",
	"about" => "О нас",
	"login" => "Логин",
	"register" => "Регистрация",
	"profile" => "Профиль",
	"logout" => "Выйти",
	"edit_profile" => "Редактировать профиль",
	"admin" => "Админ панель",

];