<?php

return [

	"home" => "Home",
	"articles" => "Articles",
	"images" => "Images",
	"contact" => "Contact",
	"about" => "About",
	"login" => "Login",
	"register" => "Register",
	"profile" => "Profile",
	"logout" => "Logout",
	"edit_profile" => "Edit profile",
	"admin" => "Admin panel",

];