@extends('app')

@section('wellcome')
	<section class="intro" id="intro" data-type="video" data-source="kn-1D5z3-Cs" data-on-error="/123/rainy-weather-hd-wallpaper1.jpg" data-mute="true" data-start="0" data-overlay="0.4">
		<div class="container">
			<div class="content">
				<div class="text-right">
					<div class="row">
						<div class="col-md-12">
							<div class="video-control animate text-center" id="video-mode">
								<i class="fa fa-play"></i>
							</div>
							<header>
								<h1 class="offsetTopClear animate">Laplas Photography site</h1>
								<h2 class="text-light animate">Made in Ukraine</h2>
							</header>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Magic Mouse :) -->
{{-- 		<div class="mouse hidden-xs">
			<div class="wheel"></div>
		</div> --}}
	</section>
{{-- 	<!-- About -->
	<section class="section offsetTop" id="about">
		<!-- Container -->
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<header class="text-center offsetBottom">
						<!-- Icon (Font-Awesome) or Image -->
						<div class="icon"><i class="fa fa-leaf fa-5x"></i></div>
						<h2>About site</h2>
					</header>
				</div>
			</div>
			<div class="row offsetBottomL">
				<div class="col-md-10 col-md-offset-1 text-center">
					<p>
						Prodo is loaded with useful, functional options that allow users to quickly and easily create
						stunning websites.<br>
						But that's not all, when you buy Prodo you will also get the most amazing customer support!
					</p>
				</div>
			</div>
		</div>
		<!-- Delimiter -->
		<hr>
		<!-- Services -->
		<div class="container">
			<div class="row services">
				<!-- Cell 1 -->
				<div class="col-md-4 col-sm-4 col-xs-6 text-center offsetTopL offsetBottomL animation animation-from-left">
					<div>
						<i class="fa fa-file-text-o"></i>
						<header>
							<h4>Articles</h4>
						</header>
						<p>Nunc condimentum purus eget lectus sagittis, in blandit justo fringilla. Pellentesque tellus nisl, efficitur in nibh vel, suscipit faucibus mi.</p>
					</div>
				</div>
				<!-- Cell 2 -->
				<div class="col-md-4 col-sm-4 col-xs-6 text-center offsetTopL offsetBottomL animation animation-from-left">
					<div>
						<i class="fa fa-picture-o"></i>
						<header>
							<h4>Images</h4>
						</header>
						<p>Nunc condimentum purus eget lectus sagittis, in blandit justo fringilla. Pellentesque tellus nisl, efficitur in nibh vel, suscipit faucibus mi.</p>
					</div>
				</div>
				<!-- Cell 3 -->
				<div class="col-md-4 col-sm-4 col-xs-6 text-center offsetTopL offsetBottomL animation animation-from-left">
					<div>
						<i class="fa fa-users"></i>
						<header>
							<h4>Users</h4>
						</header>
						<p>Nunc condimentum purus eget lectus sagittis, in blandit justo fringilla. Pellentesque tellus nisl, efficitur in nibh vel, suscipit faucibus mi.</p>
					</div>
				</div>
			</div>
		</div>
	</section> --}}

{{-- 	<section id="portfolio" class="section offsetTop">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
						<h2>Favorite Images</h2>
				</div>
			</div>
			<!-- Filters -->
			<div class="row">
				<div class="col-md-12 portfolio-filters">
					<a class="active" data-filter="*" href="#">All</a>
					<a data-filter=".photography" href="#" class="">Photography</a>
					<a data-filter=".illustration" href="#" class="">Illustration</a>
					<a data-filter=".branding" href="#" class="">Branding</a>
					<a data-filter=".personal" href="#">Personal</a>
				</div>
			</div>
		</div>
		<!-- Content -->
		<div class="container-fluid offsetTop">
			<div data-on-line-xs="2" data-on-line-sm="4" data-on-line-md="5" data-on-line-lg="5"
				 class="row portfolio-items clearfix isotope"
				 style="position: relative; overflow: hidden; height: 333px;">
				@foreach($images as $image)
						<!-- Project {{$image->id}} -->
				<div class="animation-from-left">
					<div rel="item-one" class="portfolio-item {{$image->tag}} isotope-item"
						 style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1); opacity: 1; width: 333px; height: 333px;">
						<img alt="Project" src="{{$image->file}}">
						<div class="overlay"></div>
						<div class="details">{{$image->caption}}</div>
						<a data-url="image/{{$image->id}}" href="image/{{$image->id}}"></a>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</section> --}}
{{-- 	<section id='articles' class="section offsetTop">
		<div class="container">
			<h2>Latest Articles</h2>
			@foreach($articles as $article)
				<h3>{{$article->title}}</h3>
				<p>{!!$article->body!!}</p>
			@endforeach
		</div>
	</section> --}}
@endsection


@section('js')
<script src="{{ asset('/user_panel/js/jquery.mb.ytplayer.js') }}"></script>
<script src="{{ asset('/user_panel/js/jquery.isotope.min.js') }}"></script>
@endsection