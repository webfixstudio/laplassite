@extends('app')

@section('content')
	@if ( Session::has('mes.flash_message') )
		<div class="alert {{ Session::get('mes.flash_type') }}" role="alert">
			<h3>{!!Session::get('mes.flash_message')!!}</h3>
		</div>
	@endif
	<div class="col-md-10 col-md-offset-1">
		<h2>About</h2>
		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
			industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
			scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into
			electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of
			Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
			Aldus PageMaker including versions of Lorem Ipsum.</p>

		{!! Html::image('user_panel/img/picture.jpg', 'Laravel Team', ['class'=>'img-responsive img-rounded']) !!}

		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
			industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
			scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into
			electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of
			Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
			Aldus PageMaker including versions of Lorem Ipsum.</p>
	</div>
@endsection
@section('footer')
	<section id="maps" class="section offsetTop">
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d5355.783471843962!2d35.116417388459965!3d47.84169070700738!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sua!4v1434094267291" width="100%" height="350" frameborder="0" style="border:0"></iframe>
	</section>
@endsection
@section('js')
<!-- Google Maps Support -->
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
@endsection