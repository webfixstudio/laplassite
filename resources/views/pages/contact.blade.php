@extends('app')
@section('content')
    <div class="col-md-10 col-md-offset-1">
    <h2>Contact </h2>
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
        standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
        a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
        remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing
        Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions
        of Lorem Ipsum.</p>
    <!-- Username Form Input -->

    {!! Form:: open(array('route' => 'contact.send', 'class'=>'form')) !!} <!--contact_request is a router from Route class-->

    <ul class="errors">
        @foreach($errors->all('<li>:message</li>') as $message)
            {!! $message !!}
        @endforeach
    </ul>
<div class="col-md-6">
    <div class="form-group">
        {!! Form:: label ('first_name', 'First Name *' )!!}
        {!! Form:: text ('first_name', null,
        ['required',
         'class'=>'form-control',
         'placeholder'=>'First Name'])!!}
    </div>

    <div class="form-group">
        {!! Form:: label ('last_name', 'Last Name *' )!!}
        {!! Form:: text ('last_name', null,
        ['required',
         'class'=>'form-control',
         'placeholder'=>'Last Name'] )!!}
    </div>
</div>
    <div class="col-md-6">
    <div class="form-group">
        {!! Form:: label ('phone_number', 'Phone Number' )!!}
        {!! Form:: text ('phone_number', null, [
            'class'=>'form-control',
            'placeholder' => '+38(XXX)XXX-XX-XX'
        ]) !!}
    </div>

    <div class="form-group">
        {!! Form:: label ('email', 'E-mail Address*') !!}
        {!! Form:: email ('email', null,
        ['required',
         'class'=>'form-control',
         'placeholder'=>'Your e-mail address']) !!}
    </div>

    <div class="form-group">
        {!! Form:: label ('subject', 'Subject') !!}
        {!! Form:: select ('subject', [
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4'], '1',[
                'class'=>'form-control'
        ] ) !!}
    </div>
</div>
    <div class="form-group">
        {!! Form:: label ('message', 'Message *' )!!}
        {!! Form:: textarea ('message', null,
        ['required',
         'class'=>'form-control',
         'placeholder'=>'Enter your massage... '])!!}
    </div>
    <div class="form-group">
        {!! Form::button('<i class="fa fa-envelope-o"></i> Send', ['type' => 'submit', 'class'=> 'btn btn-primary' ])!!}
        {!! Form::button('<i class="fa fa-refresh"></i> Clear', ['type' => 'reset', 'class'=> 'btn btn-danger' ])!!}
    </div>
    </div>
@stop

@section('footer')

@endsection
