@extends('admin.adminpanel')
@section('content')
	<div class="block-flat">
		<div class="header">
			<a href="{{ route('admin_tags') }}" class="btn btn-primary btn-lg"><i class="fa fa-arrow-left "></i>   Back</a>
		</div>
		<div class="content">
			<h3>Articles with tag: {{$tag->name}} </h3>
			<table class="no-border">
				<thead class="no-border">
				<tr>
					<th style="width:5%;">#ID</th>
					<th>Title</th>
					<th class="text-right">Control</th>
				</tr>
				</thead>
				<tbody class="no-border-x no-border-y">
				@foreach($tags as $tag)
					<tr>
						<td>{{$tag->article_id}}</td>
						<td>{{$tag->title}}</td>
						<td class="text-right">
							<a href="{{ route('admin_article.show', $tag->article_id) }}" class="btn btn-primary" role="button">
								<i class="fa fa-eye"></i>
							</a>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>

@endsection