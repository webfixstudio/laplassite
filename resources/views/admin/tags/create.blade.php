@extends('admin.adminpanel')

@section('content')

    <div class="block-flat">
        <div class="header">
            <h3><i class="fa fa-plus-square" style="color: #54A754"></i> Add Category</h3>
        </div>
        <div class="content">

            {!! Form::open(['url' => 'admin/tags/store']) !!}

                    <!-- Name Form Input -->
            <div class="form-group">
                {!! Form::label('name', 'Name:') !!}
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>

            <hr>
            <div class="form-group clearfix">
                {!! Form::submit('Create Tag',['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>
    </div>

@endsection
