@extends('admin.adminpanel')

@section("content")

	<div class="block-flat">
		<div class="header">
			<a href="{{ route('admin_tag.create') }}" class="btn btn-success btn-lg"><i class="fa fa-plus-square"></i>   Create</a>
		</div>
		<div class="content">
			<h3>Tags</h3>
			<table class="no-border">
				<thead class="no-border">
				<tr>
					<th style="width:5%;" class="text-center"># ID</th>
					<th style="width:39%;" class="text-center">Name</th>
					<th style="width:12%;" class="text-center">Control</th>
				</tr>
				</thead>
				<tbody class="no-border-x">
				@foreach($tags as $tag)
					<tr>
						<th class="text-center">{{$tag->id}}</th>
						<th><h3>{{$tag->name}}</h3></th>
						<th class="text-center">
							<a href="{{ route('admin_tag.show', $tag->id) }}" class="btn btn-primary" role="button">
								<i class="fa fa-eye"></i>
							</a>
							<a href="{{ route('admin_tag.edit', $tag->id) }}" class="btn btn-warning" role="button">
								<i class="fa fa-pencil-square-o"></i>
							</a>
							<a href="{{ route('admin_tag.destroy') }}" class="btn btn-danger" role="button">
								<i class="fa fa-trash-o"></i>
							</a>
						</th>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection


