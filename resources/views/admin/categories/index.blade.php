@extends('admin.adminpanel')

@section("content")
	<div class="block-flat"> 
		<div class="header">
			<a href="{{ route('admin_category.create')}}" class="btn btn-success btn-lg"><i class="fa fa-plus-square"></i>   Create</a>
		</div>
		<div class="content">
			<h3>Categories</h3>
			<table class="no-border">
				<thead class="no-border">
				<tr>
					<th style="width:5%;" class="text-center">Id/Count</th>
					<th style="width:15%;" class="text-center">Image</th>
					<th style="width:25%;" class="text">Name</th>
					<th style="width:35%;" class="text">Description</th>
					<th style="width:25%;" class="text-center">Control</th>
				</tr>	
				</thead>
				<tbody class="no-border-x">
				@foreach($categories as $category)
					<tr>
						<th class="text-center">{{$category->id}}/{{$category->articles->count()}}</th>
						<th class="text-center">
							<img src="/files/category_images/{{$category->category_image}}" class="img-rounded" style="width: 64px; margin: 15px 0;"></th>
						<th>
							<h4>{{$category->category_name}}</h4>
						</th>
						<th>
							<p>
								@if(strlen($category->description)>120)
									{!!strip_tags(substr($category->description, 0, strrpos(substr($category->description,0, 120), ' ')))!!}...
								@else
									{!! strip_tags($category->description) !!}
								@endif
							</p>
						</th>
						<th class="text-center">
							<a href="{{route('admin_category.show', [$category->id])}}" class="btn btn-primary" role="button">
								<i class="fa fa-eye"></i>
							</a>
							<a href="{{route('admin_category.edit', [$category->id])}}" class="btn btn-warning" role="button">
								<i class="fa fa-pencil-square-o"></i>
							</a>
							<a href="{{ route('admin_category.destroy', $category->id) }}" class="btn btn-danger" role="button">
								<i class="fa fa-trash-o"></i>
							</a>
						</th>
					</tr>
				@endforeach

				</tbody>
			</table>
		</div>
	</div>
@endsection

@section('css')
	<link type="text/css" rel="stylesheet" href="{{ asset('/admin_panel/js/prettify/prettify.css') }}"/>
@endsection

@section('js')
	<script type="text/javascript" src="{{ asset('/admin_panel/js/prettify/run_prettify.js') }}"></script>
@endsection