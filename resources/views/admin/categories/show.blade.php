@extends('admin.adminpanel')
@section('content')
	<div class="block-flat">
		<div class="header">
			{{-- <h3>{{$category->category_name}} </h3> --}}
			<a href="{{ route('admin_categories')}}" class="btn btn-primary btn-lg"><i class="fa fa-arrow-left "></i>Back</a>
		</div>
		<div class="content">
			<div class="row">
				<div class="col-sm-3 col-md-3">
					<img class="img-rounded" style="width: 100%" src="/files/category_images/{{$category->category_image}}" alt="Image {{$category->category_image}}">
					{{-- {{$category->category_image}} --}}
				</div>
				<div class="col-sm-9 col-md-9">
					<h2>{{$category->category_name}}</h2>
					<p>{{$category->category_alias}}</p>
					{!!$category->description!!}
				</div>
			</div>
		</div>
	</div>

	{{-- ------------------------------------------------------------------ --}}
	<div class="col-sm-12 col-md-12">
		<div class="block-flat">
			<div class="header">
				<h3>Articles</h3>
			</div>
			<div class="content">
				@foreach($category->articles as $article)
					<h3>{{$article->title}}</h3>
					<p>@if(strlen($article->body)>500)
							{!!strip_tags(substr($article->body, 0, strrpos(substr($article->body,0, 500), ' ')))!!}...
						@else
							{!! strip_tags($article->body) !!}
						@endif
					</p>
					<p class="text-left inline">{{$article->created_at}}</p><p class="text-right"> <a href="articles/{{$article->id}}">Read more...</a>
					</p>
					<hr>
				@endforeach
			</div>
		</div>
	</div>

@endsection