@extends('admin.adminpanel')

@section("content")
	<div class="block-flat">
		<div class="header">
			<h3><i class="fa fa-plus-square" style="color: #54A754"></i> Add Category</h3>
		</div>
		<div class="content">
			{!! Form::open(['route' => 'category.store', 'files' => true, 'method' => 'POST']) !!}
			<div class="form-group">
				{!! Form::label('category_name', 'Name:') !!}
				{!! Form::text('category_name', null, ['class' => 'form-control']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('parent_id', 'Parent:') !!}
				<select id="parent_id" name="parent_id">
					@foreach ($categories as $category)
						<option value="{{$category->id}}">{{ $category->category_name }}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				{!! Form::label('category_alias', 'Alias:') !!}
				{!! Form::text('category_alias', null, ['class' => 'form-control']) !!}
			</div>
			<div class="form-group">
				{!!  Form::label('description', 'Description:') !!}
				{!!  Form::textarea('description', null, ['class' => 'form-control f4']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('category_image', 'Image:') !!}
				{!! Form::file('category_image') !!}
			</div>

			<h3>Meta Data
				<button type="button" class="btn btn-default show-meta-data"><i class="fa fa-plus"></i></button>
			</h3>
			<div class="meta-data" style="display: none">
				<div class="form-group">
					{!! Form::label('meta_title', 'Meta Title:') !!}
					{!! Form::text('meta_title', null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('meta_keywords', 'Meta Keywords:') !!}
					{!! Form::text('meta_keywords', null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					{!!  Form::label('meta_description', 'Meta Description:') !!}
					{!!  Form::textarea('meta_description', null, ['class' => 'form-control']) !!}
				</div>
			</div>
			<hr>
			<div class="form-group clearfix">
				{!! Form::submit('Create Category',['class' => 'btn btn-primary']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection

@section('js')
	<script type="text/javascript" src="{{asset('/js/tinymce/tinymce.min.js')}}"></script>
	<script type="text/javascript">
		tinymce.init({
			selector: ".f4",
			plugins: [
				"advlist autolink lists link image charmap print preview anchor",
				"searchreplace visualblocks code fullscreen",
				"insertdatetime media table contextmenu paste jbimages"],
			toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
// ===========================================
// SET RELATIVE_URLS to FALSE (This is required for images to display properly)
// ===========================================
			relative_urls: false

		});
	</script>
	<script type="text/javascript">
		$(".show-meta-data").click(function () {
			$(".meta-data").toggle("blind");
		});
	</script>

@endsection