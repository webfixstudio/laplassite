@extends('admin.adminpanel')

@section("content")
	<div class="block-flat">
		<div class="header">
			<a href="{{ route('admin_image.create')}}" class="btn btn-success btn-lg"><i class="fa fa-plus-square"></i>   Add Image</a>
		</div>
		<div class="content">
			<h3>Images</h3>
			<table class="no-border">
				<thead class="no-border">
				<tr>
					<th style="width:5%;" class="text-center"># ID</th>
					<th style="width:5%;" class="text-center">Image</th>
					<th style="width:10%;" class="text-center">Name</th>
					<th style="width:25%;" class="text-center">Description</th>
					<th style="width:12%;" class="text-center">Control</th>
				</tr>
				</thead>
				<tbody class="no-border-x">
				@foreach($images as $image)
					<tr>
						<th class="text-center">{{$image->id}}</th>
						<th><img src="/{{$image->file}}" style="width: 100px; padding: 10px 0"></th>
						<th><h3>{{$image->caption}}</h3></th>
						<th><h4>{{$image->description}}</h4></th>
						<th class="text-center">
							<a href="{{ route('admin_image.show', $image->id) }}" class="btn btn-primary" role="button">
								<i class="fa fa-eye"></i>
							</a>
							<a href="{{ route('admin_image.edit', $image->id) }}" class="btn btn-warning" role="button">
								<i class="fa fa-pencil-square-o"></i>
							</a>
							<a href="{{ route('admin_image.destroy', $image->id) }}" class="btn btn-danger" role="button">
								<i class="fa fa-trash-o"></i>
							</a>
						</th>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection


