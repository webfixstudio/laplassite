@extends('admin.adminpanel')
@section('content')
    <div class="block-flat">
        <div class="header">

            <h3>Articles with tag </h3>
        </div>
        <div class="content">
            <a href="{{ url('admin/images/index')}}" class="btn btn-primary btn-lg"><i class="fa fa-arrow-left "></i>
                Back</a>
            <div>
            <img src="/{{$image->file}}" style="width: auto">
            {{$image->caption}}
            {{$image->description}}
            </div>
            <table class="no-border">
                <thead class="no-border">
                <tr>
                    <th style="width:5%;">#ID</th>
                    <th>Title</th>
                    <th>Control</th>
                </tr>
                </thead>
                <tbody class="no-border-x no-border-y">
                    <tr>
                        <td><img src="/{{$image->file}}" style="width: 100px"> </td>
                        <td>{{$image->caption}}</td>
                        <td>{{$image->description}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection