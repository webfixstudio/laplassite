@extends('admin.adminpanel')

@section("content")

    <div class="block-flat">
        <div class="header">
            <h3><i class="fa fa-pencil-square-o " style="color: #54A754"></i> Edit Tags</h3>
        </div>
        <div class="content">

            {!! Form::model($tag, ['url' => 'admin/tags/update', 'metod'=>'POST', 'files'=>true]) !!}
            <!-- Name Form Input -->
            <div class="form-group">
                {!! Form::label('name', 'Name:') !!}
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>

            <hr>
            <div class="form-group clearfix">
                {!! Form::submit('Edit Category',['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>
    </div>

@endsection

