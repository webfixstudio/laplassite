@extends('admin.adminpanel')

@section('content')
	<div class="block-flat">
		<div class="header">
			<h3><i class="fa fa-plus-square" style="color: #54A754"></i> Add Image</h3>
		</div>
		<div class="content">
			{!! Form::open(['route'=>'admin_image.store', 'method'=>'POST', 'files'=>'true']) !!}
			<div class="form-group">
				 <label for="image">Image File</label>
				 <input type="file" name="image">
			</div>
			<div class="form-group">
				 <label for="tag">Tag</label>
				 {!!Form::select('tag', ['photography'=>'Photography', 'illustration'=>'Illustration', 'branding'=>'Branding', 'personal'=>'Personal'], 'personal',['class'=>'form-control'])!!}
			</div>
			<div class="form-group">
				 <label for="caption">Caption</label>
				 <input type="text" class="form-control" name="caption" value="">
			</div>
			<div class="form-group">
				 <label for="description">Description</label>
				 <textarea class="form-control" name="description"></textarea>
			</div>

			<button type="submit" class="btn btn-primary">Create</button>
			{{-- <a href="{{ route('images') }}" class="btn btn-warning">Cancel</a> --}}
	 		{!! Form::close() !!}
		</div>
	</div>
@endsection
