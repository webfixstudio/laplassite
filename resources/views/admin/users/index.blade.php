@extends('admin.adminpanel')
@section('content')
	<div class="block-flat">
		<div class="header">
			<h3>Users</h3>
		</div>
		<div class="content">
			{{-- <a href="{{ route('ausers') }}" class="btn btn-success btn-lg"><i class="fa fa-plus-square"></i> r</a> --}}
			<table class="no-border">
				<thead class="no-border">
				<tr>
					<th style="width: 5%" class="text-center"># ID</th>
					<th style="width: 15%" class="text-center">Photo</th>
					<th class="text">Name</th>
					<th class="text">E-mail</th>
					<th style="width:25%;" class="text-center">Control</th>
				</tr>
				</thead>
				<tbody class="no-border-x">             
				@foreach($allusers as $user)
					<tr>
						<th class="text-center">{{$user->id}}</th>
						<th class="text-center">
							<img src="{{'/files/users_images/'.$user->foto}}" class="img-rounded" style="width: 64px; margin: 15px 0 ">
							</th>
						<th><p>{{$user->name}}</p></th>
						<th><p>{{$user->email}}</p></th>
						<th class="text-center">
							<a href="{{ route('admin_user.show',$user->id)}}" class="btn btn-primary" role="button">
								<i class="fa fa-eye"></i>
							</a>
							<a href="{{ route('admin_user.edit',$user->id)}}" class="btn btn-warning" role="button">
								<i class="fa fa-pencil-square-o"></i>
							</a>
							<a href="{{ route('admin_user.destroy',$user->id)}}" class="btn btn-danger" role="button">
								<i class="fa fa-trash-o"></i>
							</a>
						</th>
					</tr>
				@endforeach

				</tbody>
			</table>
		</div>
	</div>

@endsection