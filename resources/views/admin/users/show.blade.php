@extends('admin.adminpanel')
@section('content')
	<div class="block-flat">
		<div class="header">
			<a href="{{ route('ausers')}}" class="btn btn-primary btn-lg"><i class="fa fa-arrow-left "></i>Back</a>
			<a href="{{ url('admin/categories/add')}}" class="btn btn-success btn-lg"><i class="fa fa-plus-square"></i>   Edit</a>
		</div>
		<div class="content">
			<div class="row">
				<div class="col-sm-4 col-md-4">
					<img class="img-rounded" style="width: 100%" src="/files/users_images/{{$user->foto}}" alt="Image {{$user->name}}">
				</div>
				<div class="col-sm-8 col-md-8">
					<h2>{{$user->name}}</h2>
					<table class="table"> 
						<tbody class="table-striped">
							<tr>
								<th>Email:</th>
								<th>{{$user->email}}</th>
							</tr>	
							<tr>
								<th>Phone:</th>
								<th>{{$user->phone}}</th>
							</tr>
							<tr>
								<th>Facebook:</th>
								<th>{{$user->facebook}}</th>
							</tr>
							<tr>
								<th>Skype:</th>
								<th>{{$user->skype}}</th>
							</tr>
							<tr>
								<th>Birthday:</th>
								<th>{{$user->birthday}}</th>
							</tr>
							<tr>
								<th>Created:</th>
								<th>{{$user->created_at}}</th>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	{{-- ------------------------------------------------------------------ --}}
	<div class="col-sm-12 col-md-12">
		<div class="block-flat">
			<div class="header">
				<h3>Articles</h3>
			</div>
			<div class="content">
				@foreach($user->articles as $article)
					<h3>{{$article->title}}</h3>
					<p>@if(strlen($article->body)>500)
							{!!strip_tags(substr($article->body, 0, strrpos(substr($article->body,0, 500), ' ')))!!}...
						@else
							{!! strip_tags($article->body) !!}
						@endif
					</p>
					<p class="text-left inline">{{$article->created_at}}</p><p class="text-right"> <a href="articles/{{$article->id}}">Read more...</a>
					</p>
					<hr>
				@endforeach
			</div>
		</div>
	</div>


@endsection