@extends('admin.adminpanel')
@section('content')
	<div class="block-flat">
		<div class="header">
			<a href="{{ route('admin_articles') }}" class="btn btn-default btn-lg"><i class="fa fa-arrow-left"></i>   Back</a>
		</div>
		<div class="content">
			<h3>Add Article:</h3>
			{!! Form::open(['route' => 'admin_article.store']) !!}
			<div class="form-group">
				{!! Form::label('title', 'Title:') !!}
				{!! Form::text('title', null, ['class' => 'form-control']) !!}
			</div>
			<div class="form-group">
				{!!  Form::label('body', 'Body:') !!}
				{!!  Form::textarea('body', null, ['class' => 'form-control']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('category_list', 'Categories:') !!}
				{!! Form::select('category_list[]', $category, null, ['class' => 'form-control','multiple']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('published_at', 'Published On:') !!}
				{!! Form::date('published_at', null, ['class'=>'form-control']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('tag_list', 'Tags:') !!}
				{!! Form::select('tag_list[]', $tags, null, ['class' => 'form-control','multiple']) !!}
			</div>
			{!! Form::submit('Create Article',['class' => 'btn btn-primary']) !!}
		{!! Form::close() !!}


{{-- 			{!! Form::open(['url' => 'articles']) !!}

				<!-- Title Form Input -->
				<div class="form-group">
					{!! Form::label('title', 'Title:') !!}
					{!! Form::text('title', null, ['class' => 'form-control']) !!}
				</div>
				<!-- Body Form Input -->
				<div class="form-group">
					{!!  Form::label('body', 'Body:') !!}
					{!!  Form::textarea('body', null, ['class' => 'form-control']) !!}
				</div>
				<!-- Published_at Form Input -->
				<div class="form-group">
					{!! Form::label('published_at', 'Published On:') !!}
					{!! Form::input('date', 'published_at', date('d-m-Y'), ['class' => 'form-control datepicker']) !!}
				</div>
				<!-- Tags Form Input -->
				<div class="form-group">
					{!! Form::label('tag_list', 'Tags:') !!}
					{!! Form::select('tag_list[]', $tags, null, ['class' => 'tags','multiple']) !!}
					{!! Form::input('tag_list[]', $tags, null, ['class' => 'tags','multiple']) !!}
					<input class="tags" type="hidden" value="{{$tags}}" /> 
				</div>
				{!! Form::submit('Save',['class' => 'btn btn-primary btn-lg']) !!}
			{!! Form::close() !!} --}}

			@include('errors.list')

			@section('js')
				<script type="text/javascript" src="{{asset('/user_panel/js/tinymce/tinymce.min.js')}}"></script>
				<script type="text/javascript">
					tinymce.init({
						selector: "textarea",
						plugins: [
							"advlist autolink lists link image charmap print preview anchor",
							"searchreplace visualblocks code fullscreen",
							"insertdatetime media table contextmenu paste jbimages"],
						toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
						// ===========================================
						// SET RELATIVE_URLS to FALSE (This is required for images to display properly)
						// ===========================================
						relative_urls: false
					});
				</script>
				<script type="text/javascript">
					$('select').select2({
						placeholder: 'Choose a tag',
						tags:true
					});
				</script>

				<script type="text/javascript" src="{{asset('/admin_panel/js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
				<script src="{{asset('/admin_panel/js/jquery.maskedinput/jquery.maskedinput.js')}}" type="text/javascript"></script>
				<script type="text/javascript">    /*Form Masks*/
					$(document).ready(function(){
						$("[data-mask='date']").mask("99/99/9999");
						$("[data-mask='phone']").mask("(999) 999-9999");
						$("[data-mask='phone-ext']").mask("(999) 999-9999? x99999");
						$("[data-mask='phone-int']").mask("+33 999 999 999");
						$("[data-mask='taxid']").mask("99-9999999");
						$("[data-mask='ssn']").mask("999-99-9999");
						$("[data-mask='product-key']").mask("a*-999-a999");
						$("[data-mask='percent']").mask("99%");
						$("[data-mask='currency']").mask("$999,999,999.99");
					});//End of masks
				</script>
				<script type="text/javascript">
					$("[data-mask='phone']").cleanVal();
				</script>

				<script type="text/javascript" src="{{asset('/js/tinymce/tinymce.min.js')}}"></script>
				<script type="text/javascript">
					tinymce.init({
						selector: "textarea",
						plugins: [
							"advlist autolink lists link image charmap print preview anchor",
							"searchreplace visualblocks code fullscreen",
							"insertdatetime media table contextmenu paste jbimages"],
						toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
// ===========================================
// SET RELATIVE_URLS to FALSE (This is required for images to display properly)
// ===========================================
						relative_urls: false

					});
				</script>
				<script type="text/javascript">
					$('select').select2({
						placeholder: 'Choose a tag',
						tags:true
					});
				</script>

			@endsection
		</div>
	</div>

@endsection