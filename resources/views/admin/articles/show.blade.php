@extends('admin.adminpanel')

@section('content')
	<div class="block-flat">
		<div class="header">
			<a href="{{ route('admin_articles') }}" class="btn btn-default btn-lg">
				<i class="fa fa-arrow-left"></i>   Back
			</a>
			<a href="{{ route('admin_article.edit', $article->id) }}" class="btn btn-warning btn-lg">
				<i class="fa fa-pencil-square-o"></i>   Edit
			</a>
			<a href="{{ route('admin_article.create') }}" class="btn btn-success btn-lg"><i class="fa fa-plus-square"></i>   Create</a>
		</div>
		<div class="content">
			<h3>{{$article->title}}</h3>
			<small>{{$article->user->name}}</small>
			<div>{!!$article->body!!}</div>
		</div>
	</div>

@endsection