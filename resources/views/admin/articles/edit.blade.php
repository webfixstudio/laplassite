@extends('admin.adminpanel')

@section("content")
	<div class="block-flat">
		<div class="header">
		<a href="{{ route('admin_articles') }}" class="btn btn-default btn-lg"><i class="fa fa-arrow-left"></i>   Back</a>
		</div>
		<div class="content">
			<h3>Edit: {{$article->title}}</h3>

			{!! Form::model($article, ['route' => 'admin_article.update', 'metod'=>'PUT', 'files'=>true]) !!}
				<div class="form-group">
					{!! Form::label('title', 'Title:') !!}
					{!! Form::text('title', null, ['class' => 'form-control']) !!}
				</div>
				<!-- Body Form Input -->
				<div class="form-group">
					{!!  Form::label('body', 'Body:') !!}
					{!!  Form::textarea('body', null, ['class' => 'form-control']) !!}
				</div>
				<!-- Published_at Form Input -->
				<div class="form-group">
					{!! Form::label('published_at', 'Published On:') !!}
					{!! Form::input('date', 'published_at', $article->created_at, ['class' => 'form-control datepicker']) !!}
				</div>
				<!-- Tags Form Input -->
				<div class="form-group">
					{!! Form::label('tag_list', 'Tags:') !!}
					{!! Form::select('tag_list[]', $article->tags, null, ['class' => 'form-control','multiple']) !!}
				</div>
				{!! Form::submit('Update',['class' => 'btn btn-primary btn-lg']) !!}
			{!! Form::close() !!}
		</div>
	</div>

@endsection

