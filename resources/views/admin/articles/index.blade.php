@extends('admin.adminpanel')
@section('content')
	<div class="block-flat">
		<div class="header">
			<a href="{{ route('admin_article.create') }}" class="btn btn-success btn-lg"><i class="fa fa-plus-square"></i>   Create</a>
		</div>
		<div class="content">
			<h3>Articles</h3>
			<table class="no-border">
				<thead class="no-border">
				<tr>
					<th class="text-center"># ID</th>
					<th class="text-center">Title</th>
					<th class="text-center">Body</th>
					<th class="text-center">Created</th>
					<th style="width:15%;" class="text-center">Control</th>
				</tr>
				</thead>
				<tbody class="no-border-x">             
				@foreach($articles as $article)
					<tr>
						<th class="text-center">{{$article->id}}</th>
						<th class="text-center">{{$article->title}}</th>
						<th>{{substr($article->body, 0, strrpos(substr($article->body,0, 140), ' '))}}</th>
						<th class="text-center"><p>{{$article->created_at}}</p></th>
						<th class="text-center">
							<a href="{{ route('admin_article.show', $article->id) }}" class="btn btn-primary" role="button">
								<i class="fa fa-eye"></i>
							</a>
							<a href="{{ route('admin_article.edit', $article->id) }}" class="btn btn-warning" role="button">
								<i class="fa fa-pencil-square-o"></i>
							</a>
							<a href="{{ route('admin_article.destroy', $article->id) }}" class="btn btn-danger" role="button">
								<i class="fa fa-trash-o"></i>
							</a>
						</th>
					</tr>
				@endforeach

				</tbody>
			</table>
		</div>
	</div>
@endsection
