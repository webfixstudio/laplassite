@if ($errors->any())

    @foreach($errors->all() as $error)
        <div class="alert alert-danger alert-white rounded">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <div class="icon"><i class="fa fa-times-circle"></i></div>
            <strong>Error!</strong> {{$error}}
        </div>

    @endforeach

@endif


