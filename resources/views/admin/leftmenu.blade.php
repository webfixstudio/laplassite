<div class="cl-sidebar">
	<div class="cl-toggle"><i class="fa fa-bars"></i></div>
	<div class="cl-navblock">
		<div class="menu-space">
			<div class="content">
				<div class="side-user">
					<div class="avatar"><img src="{{ url('/files/users_images/'.Auth::user()->foto) }}" alt="Avatar" style="width: 196px"/></div>
					<div class="info">
						<a href="{{ url('/user/'.Auth::user()->id) }}">{{ Auth::user()->name }}</a>
						<img src="/admin_panel/images/state_online.png" alt="Status"/> <span>Online</span>
					</div>
				</div>
				<ul class="cl-vnavigation">
					<li><a href="{{ route('admin') }}"><i class="fa fa-home"></i><span>Dashboard</span></a></li>
					<li><a href="{{ route('admin_articles') }}"><i class="fa fa-file-text-o"></i><span>Articles</span></a></li>
					<li><a href="{{ route('admin_categories') }}"><i class="fa fa-bookmark"></i><span>Categories</span></a></li>
					<li><a href="{{ route('admin_users') }}"><i class="fa fa-users"></i><span>Users</span></a></li>
					<li><a href="{{ route('admin_tags') }}"><i class="fa fa-cloud"></i><span>Tags</span></a></li>
					<li><a href="{{ route('admin_images') }}"><i class="fa fa-cloud"></i>Images</a></li>
				</ul>
			</div>
		</div>
		<div class="text-right collapse-button" style="padding:7px 9px;">
			<button id="sidebar-collapse" class="btn btn-default" style="">
				<i style="color:#fff;" class="fa fa-angle-left"></i>
			</button>
		</div>
	</div>
</div>