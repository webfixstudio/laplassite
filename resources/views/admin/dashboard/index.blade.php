@extends('admin.adminpanel')
@section('content')
	<div class="row">
		<div class="col-md-4">
			<div class="block-flat">
				<div class="content no-padding">
					<div class="overflow-hidden">
						<i class="fa fa-users fa-4x pull-left color-primary"></i>

						<h3 class="no-margin">TOTAL USERS</h3>

						<p class="color-primary">Total number users on site</p>
					</div>
					<h1 class="no-margin big-text">{{count($users)}}</h1>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="block-flat">
				<div class="content no-padding">
					<div class="overflow-hidden">
						<i class="fa fa-files-o fa-4x pull-left color-warning"></i>

						<h3 class="no-margin">TOTAL ARTICLES</h3>

						<p class="color-danger">Total number articles on site</p>
					</div>
					<h1 class="big-text no-margin">{{count($articles)}}</h1>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="block-flat">
				<div class="content no-padding">
					<div class="overflow-hidden">
						<i class="fa fa-tags fa-4x pull-left color-success"></i>

						<h3 class="no-margin">TAGS</h3>

						<p class="color-success">Total number tags on site</p>
					</div>
					<h1 class="big-text no-margin">{{count($tags)}}</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="col-sm-6 col-md-6">
		<div class="block-flat">
			<div class="header">
				<h3>Users Cards</h3>
			</div>
			<div class="content">
				<div class="list-group">
					@foreach($users as $user)
						<a class="list-group-item" href="{{ route('admin_user.show',$user->id)}}">
							<div class="row" style="margin-top: 0">
							<div class="col-md-8">
								<h5 class="list-group-item-heading">{{$user->name}}</h5>
								<i class="fa fa-mobile-phone"></i>{{$user->phone}}</br>
								<i class="fa fa-envelope"></i>{{$user->email}}
							</div>
								<div class="col-md-2">
									<img class="img-rounded" src="/files/users_images/{{$user->foto}}" style="width: 100px">
								</div>
							</div>
						</a>
					@endforeach
				</div>
			</div>
		</div>
		<div class="block-flat">
			<div class="header">
				<h3>Custom Content</h3>
			</div>
			<div class="content">

				<ul class="nav nav-tabs">
					<li class="active"><a href="#home" data-toggle="tab">Home</a></li>
					<li><a href="#profile" data-toggle="tab">Profile</a></li>
					<li><a href="#messages" data-toggle="tab">Messages</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active cont" id="home">
						<h2 class="text-center">Our Experience</h2>

						<div id="piec" style="height:300px;margin-top:25px;"></div>
					</div>
					<div class="tab-pane cont" id="profile">
						<h2>Typography</h2>

						<p>Pellentesque ac quam hendrerit, viverra leo eu, <b>dapibus mi</b>. In at luctus massa. Morbi
							semper nulla eu velit facilisis pellentesque. Mauris adipiscing turpis in bibendum tempus.
							<i>Donec viverra</i>, lacus ac mollis rhoncus, libero risus placerat nisi, et viverra justo
							eros eget dui. Mauris convallis et tellus non <a href="#">placerat</a>.</p>

						<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce
							gravida est eros, eget porta leo fringilla et. </p>
						<a href="#">Read more</a>
						
					</div>
					<div class="tab-pane" id="messages">
						<h2 class="hthin">A Lorem Ipsum Story</h2>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent a metus pulvinar turpis
							porttitor imperdiet vel nec justo. Nam id orci purus. Mauris arcu velit, auctor et aliquam
							quis, rhoncus a velit. Sed laoreet ultrices dolor eget vehicula. Morbi adipiscing euismod
							nisi, eget tincidunt arcu laoreet at.</p>

						<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
							Etiam et tortor ultricies, mollis nunc eget, gravida sapien.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-sm-6 col-md-6">

		<div class="block-flat">
			<div class="header">
				<h3>Articles</h3>
			</div>
			<div class="content">

				@foreach($articles as $article)
					<h3>{{$article->title}}</h3>
					<p>@if(strlen($article->body)>500)
							{!!strip_tags(substr($article->body, 0, strrpos(substr($article->body,0, 500), ' ')))!!}...
						@else
							{!! strip_tags($article->body) !!}
						@endif
					</p>
					<p class="text-left inline">{{$article->created_at}}</p><p class="text-right"> <a href="{{ route('admin_article.show',$article->id) }}">Read more...</a>
					</p>
					<hr>
				@endforeach
			</div>
		</div>
	</div>

@endsection

@section('js')
	<script type="text/javascript" src="/admin_panel/js/jquery.flot/jquery.flot.js"></script>
	<script type="text/javascript" src="/admin_panel/js/jquery.flot/jquery.flot.pie.js"></script>
	<script type="text/javascript" src="/admin_panel/js/jquery.flot/jquery.flot.resize.js"></script>
	<script type="text/javascript" src="/admin_panel/js/jquery.flot/jquery.flot.labels.js"></script>

	<script type="text/javascript">
		/*Pie Chart*/
		var data = [
			{label: "Users", data: {{count($users)}} },
			{label: "Articles", data: {{count($articles)}} },
				@foreach($articles as $article)
									   {
				label: "{{$article->title}}", data: 20
			},

			@endforeach



		];

		$.plot('#piec', data, {
			series: {
				pie: {
					show: true,
					innerRadius: 0.27,
					shadow: {
						top: 5,
						left: 15,
						alpha: 0.3
					},
					stroke: {
						width: 0
					},
					label: {
						show: true,
						formatter: function (label, series) {
							return '<div style="font-size:12px;text-align:center;padding:2px;color:#333;">' + label + '</div>';

						}
					},
					highlight: {
						opacity: 0.08
					}
				}
			},
			grid: {
				hoverable: true,
				clickable: true
			},
			colors: ["#5793f3", "#dd4d79", "#bd3b47", "#dd4444", "#fd9c35", "#fec42c", "#d4df5a", "#5578c2"],
			legend: {
				show: false
			}
		});
	</script>

	<script src="{{asset('/admin_panel/js/111/jqvmap/jquery.vmap.js')}}"></script>
	<script src="{{asset('/admin_panel/js/111/jqvmap/jquery.vmap.ukraine.js')}}"></script>
	<link rel="stylesheet" type="text/css" href=" {{asset('/admin_panel/js/111/jqvmap/ukraine.css')}}">

@endsection
