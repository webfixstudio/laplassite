<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="/admin_panel/images/favicon.png">

	<title>Admin Zone</title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset('/admin_panel/js/bootstrap/dist/css/bootstrap.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('/admin_panel/js/jquery.gritter/css/jquery.gritter.css') }}"/>
	<link rel="stylesheet" type="text/css"
		  href="{{ asset('/admin_panel/fonts/font-awesome-4/css/font-awesome.min.css') }}"/>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="../../assets/js/html5shiv.js"></script>
	<script src="../../assets/js/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="{{ asset('/admin_panel/js/jquery.nanoscroller/nanoscroller.css') }}"/>
	<link rel="stylesheet" type="text/css"
		  href="{{ asset('/admin_panel/js/jquery.easypiechart/jquery.easy-pie-chart.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('/admin_panel/js/bootstrap.switch/bootstrap-switch.css') }}"/>
	<link rel="stylesheet" type="text/css"
		  href="{{ asset('/admin_panel/js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('/admin_panel/js/jquery.select2/select2.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('/admin_panel/js/bootstrap.slider/css/slider.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('/admin_panel/css/style.css') }}"/>
	@yield('css')
</head>
<body>
<!-- Fixed navbar -->
<div id="head-nav" class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="fa fa-gear"></span>
			</button>
			<a class="navbar-brand" href="{{ route('admin') }}"><span>Laplas</span></a>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="{{ route('admin') }}">Home</a></li>
				<li><a href="{{ route('articles') }}">Articles</a></li>
				<li><a href="{{ route('about') }}">About</a></li>
				<li><a href="{{ route('contact') }}">Contact</a></li>
			</ul>
{{-- 			<ul class="nav navbar-nav navbar-right user-nav">
				<li class="dropdown profile_menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img alt="Avatar" src="{{ url('/files/users_images/'.Auth::user()->foto) }}" style="width: 30px"/>{{Auth::user()->name}}<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="{{ url('/user/'.Auth::user()->id) }}">My Account</a></li>
						<li><a href="#">Profile <i class="fa fa-bomb"></i> </a></li>
						<li><a href="#">Messages <i class="fa fa-bomb"></i></a></li>
						<li class="divider"></li>
						<li><a href="{{ url('/auth/logout') }}">Sign Out</a></li>
					</ul>
				</li>
			</ul> --}}
			{{-- <ul class="nav navbar-nav navbar-right not-nav">
				<li class="button dropdown">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class=" fa fa-comments"></i></a>
					<ul class="dropdown-menu messages">
						<li>
							<div class="nano nscroller">
								<div class="content">
									<ul>
										<li>
											<a href="#">
												<img src="/admin_panel/images/avatar2.jpg" alt="avatar"/><span
														class="date pull-right">13 Sept.</span> <span class="name">Daniel</span>
												I'm following you, and I want your money!
											</a>
										</li>
										<li>
											<a href="#">
												<img src="/admin_panel/images/avatar_50.jpg" alt="avatar"/><span
														class="date pull-right">20 Oct.</span><span
														class="name">Adam</span> is now following you
											</a>
										</li>
										<li>
											<a href="#">
												<img src="/admin_panel/images/avatar4_50.jpg" alt="avatar"/><span
														class="date pull-right">2 Nov.</span><span
														class="name">Michael</span> is now following you
											</a>
										</li>
										<li>
											<a href="#">
												<img src="/admin_panel/images/avatar3_50.jpg" alt="avatar"/><span
														class="date pull-right">2 Nov.</span><span
														class="name">Lucy</span> is now following you
											</a>
										</li>
									</ul>
								</div>
							</div>
							<ul class="foot">
								<li><a href="#">View all messages </a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="button dropdown">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i
								class="fa fa-globe"></i><span class="bubble">2</span></a>
					<ul class="dropdown-menu">
						<li>
							<div class="nano nscroller">
								<div class="content">
									<ul>
										<li><a href="#"><i class="fa fa-cloud-upload info"></i><b>Daniel</b> is now
												following you <span class="date">2 minutes ago.</span></a></li>
										<li><a href="#"><i class="fa fa-male success"></i> <b>Michael</b> is now
												following you <span class="date">15 minutes ago.</span></a></li>
										<li><a href="#"><i class="fa fa-bug warning"></i> <b>Mia</b> commented on post
												<span class="date">30 minutes ago.</span></a></li>
										<li><a href="#"><i class="fa fa-credit-card danger"></i> <b>Andrew</b> killed
												someone <span class="date">1 hour ago.</span></a></li>
									</ul>
								</div>
							</div>
							<ul class="foot">
								<li><a href="#">View all activity </a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="button"><a href="javascript:;" class="speech-button"><i class="fa fa-microphone"></i></a>
				</li>
			</ul> --}}

		</div>
		<!--/.nav-collapse -->
	</div>
</div>

<div id="cl-wrapper">

	@include('admin.leftmenu')

	<div class="container-fluid" id="pcont">

		<div class="cl-mcont">

			@include('admin.errors.create')
			@yield('content')

		</div>

	</div>

</div>

<script type="text/javascript" src="{{ asset('/admin_panel/js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('/admin_panel/js/jquery.select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/admin_panel/js/jquery.nanoscroller/jquery.nanoscroller.js') }}"></script>
<script type="text/javascript" src="{{ asset('/admin_panel/js/jquery.sparkline/jquery.sparkline.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/admin_panel/js/jquery.easypiechart/jquery.easy-pie-chart.js') }}"></script>
<script type="text/javascript" src="{{ asset('/admin_panel/js/jquery.ui/jquery-ui.js') }}"></script>
<script type="text/javascript" src="{{ asset('/admin_panel/js/jquery.nestable/jquery.nestable.js') }}"></script>
<script type="text/javascript" src="{{ asset('/admin_panel/js/bootstrap.switch/bootstrap-switch.min.js') }}"></script>
{{-- <script type="text/javascript" src="{{ asset('/admin_panel/js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script> --}}
{{-- <script type="text/javascript" src="{{ asset('/admin_panel/js/jquery.select2/select2.min.js') }}"></script> --}}
{{-- <script type="text/javascript" src="{{ asset('/admin_panel/js/bootstrap.slider/js/bootstrap-slider.js') }}"></script> --}}
<script type="text/javascript" src="{{ asset('/admin_panel/js/jquery.gritter/js/jquery.gritter.js') }}"></script>
<script type="text/javascript" src="{{ asset('/admin_panel/js/behaviour/general.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function () {
		//initialize the javascript
		App.init();
	});
</script>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="{{ asset('/admin_panel/js/behaviour/voice-commands.js') }}"></script>
<script type="text/javascript" src="{{ asset('/admin_panel/js/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/admin_panel/js/jquery.flot/jquery.flot.js') }}"></script>
<script type="text/javascript" src="{{ asset('/admin_panel/js/jquery.flot/jquery.flot.pie.js') }}"></script>
<script type="text/javascript" src="{{ asset('/admin_panel/js/jquery.flot/jquery.flot.resize.js') }}"></script>
<script type="text/javascript" src="{{ asset('/admin_panel/js/jquery.flot/jquery.flot.labels.js') }}"></script>
@yield('js')

</body>
</html>
