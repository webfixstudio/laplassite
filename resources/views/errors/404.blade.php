<!DOCTYPE html>
<html class="no-js" lang="en-US">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Nicolay Ponomarev">
    <title>Laplas &ndash; 404 Error</title>

    <!-- Bootstrap -->
    <link href="{{ asset('/user_panel/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Modernizr -->
    <script src="{{ asset('/user_panel/js/modernizr.custom.js') }}"></script>
    <!-- Theme Styles -->
    <link href="{{ asset('/user_panel/css/plugins/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/user_panel/css/styles.css') }}" rel="stylesheet">
</head>
<body>
<!-- Loader -->
<div class="page-loader">
    <div class="content">
        <div class="line">
            <div class="progress"></div>
        </div>
        <div class="text">Loading...</div>
    </div>
</div>

<!-- Content -->
<section class="section" id="error-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <header class="text-center offsetBottomS">
                    <!-- Icon, Title -->
                    <div class="icon largest colored offsetBottomS"><i class="fa fa-chain-broken"></i></div>
                    <h1 class="offsetTopS">Something has gone wrong!</h1>
                    <!-- Primary Text -->
                    <p class="info">
                        The page you are trying to reach doesn't seem to exist.
                    </p>
                </header>
            </div>
        </div>
        <!-- Button -->
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="/" class="btn btn-default">Take me back</a>
            </div>
        </div>
    </div>
</section>

<script>
    var Prodo = {
        loader: false,
        animations: true
    };
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ asset('/user_panel/js/jquery.min.js') }}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ asset('/user_panel/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- Other Scripts -->
<script src="{{ asset('/user_panel/js/jquery.nav.js') }}"></script>
<script src="{{ asset('/user_panel/js/prodo.min.js') }}"></script>
</body>
</html>