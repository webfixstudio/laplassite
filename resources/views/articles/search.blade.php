@extends('app')

@section("content")
<div class="row">
	<div class="col-md-8 col-sm-8">
		{{-- <h1>{{trans('articles.title')}}</h1> --}}
		<h3>Поиск: {{$search}}</h3>
		<p>Найдено совпадений: {{$articles->count()}}</p>
		@if($articles)
		@foreach($articles as $article)
			<article class="row blog-post offsetBottom">
				<div class="col-md-12 col-sm-12">
					<header>
						<h2>
							<a href="{{route('article.show', $article->id)}}">{{$article->title}}</a>
						</h2>
						<div class="info">
							<span>{{$article->user->name}}</span>
							<span>
							@foreach ($article->category as $category)
								<span><a href="{{route('article.category', $category->id)}}">{{ $category->category_name}}</a></span>
							@endforeach
							</span>
							<span>{{ \Carbon\Carbon::parse($article->published_at)->format('F d, Y')}}</span>
						</div>
					</header>
					<div class="article-body">
						@if(strlen($article->body)>500)
							{!!substr($article->body, 0, strrpos(substr($article->body,0, 500), ' '))!!}...
						@else
							{!! $article->body !!}
						@endif
					</div>
					<a class="btn btn-default" href="{{route('article.show', $article->id)}}">{{trans('articles.read_more')}}</a>
				</div>
			</article>
			<hr>
			<div class="clearfix"></div>
		@endforeach
		@else
			<h2>Nothing found </h2>
		@endif
		{!! $articles->render() !!}
	</div>
	<div class="col-md-4 col-sm-4">
		@include("sidebar")
	</div>
</div>
@endsection