@extends('app')

@section("content")
<div class="row">
	<div class="col-md-8 col-sm-8">
		<h1>Create article</h1>
		<hr>
		@include('errors.list')
		{!! Form::open(['url' => 'articles']) !!}
			<div class="form-group">
				{!! Form::label('title', 'Title:') !!}
				{!! Form::text('title', null, ['class' => 'form-control']) !!}
			</div>
			<div class="form-group">
				{!!  Form::label('body', 'Body:') !!}
				{!!  Form::textarea('body', null, ['class' => 'form-control']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('category_list', 'Categories:') !!}
				{!! Form::select('category_list[]', $category, null, ['class' => 'form-control','multiple']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('published_at', 'Published On:') !!}
				{!! Form::date('published_at', null, ['class'=>'form-control']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('tag_list', 'Tags:') !!}
				{!! Form::select('tag_list[]', $tags, null, ['class' => 'form-control','multiple']) !!}
			</div>
			{!! Form::submit('Create Article',['class' => 'btn btn-primary']) !!}
		{!! Form::close() !!}
	</div>
	<div class="col-md-4 col-sm-4">
		@include("sidebar")
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="{{asset('/user_panel/js/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript">
	tinymce.init({
		selector: "textarea",
		plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste jbimages"],
		toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
		// ===========================================
		// SET RELATIVE_URLS to FALSE (This is required for images to display properly)
		// ===========================================
		relative_urls: false
	});
	$('select').select2({
		placeholder: 'Choose a tag',
		tags:true
	});
</script>
@endsection