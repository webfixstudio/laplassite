@extends('app')

@section("content")
	<div class="col-md-8 col-sm-8">
		<article class="row blog-post offsetBottom">
			<header>
				<h2>{{$article->title}}</h2>
				<div class="info">
					<span>{{$article->user->name}}</span>
					{{-- <span>{{$article->category[0]->category_name}}</span> --}}
					{{-- <span><a href="#">Demonstration</a></span> --}}
					<span>{{\Carbon\Carbon::parse($article->created_at)->format('F d, Y')}}</span>
				</div>
			</header>
			<div class="article-body">
				{!! $article->body !!}
			</div>
		</article>
		@if (Session::has('message_my'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				{{Session::get('message_my')}}
			</div>
		@endif
		<div class="col-md-12 share-panel">
			<span>Share</span>
			<div class="social">
				<a onclick="shareTo( 'twitter', '#share-title', '#share-image' )" title="Twitter"><i class="fa fa-twitter"></i></a>
				<a onclick="shareTo( 'facebook', '#share-title', '#share-image' )" title="Facebook"><i class="fa fa-facebook"></i></a>
				<a onclick="shareTo( 'pinterest', '#share-title', '#share-image' )" title="Pinterest"><i class="fa fa-pinterest"></i></a>
				<a onclick="shareTo( 'linkedin', '#share-title', '#share-image' )" title="LinkedIn"><i class="fa fa-linkedin"></i></a>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-4">
	@unless($article->tags->isEmpty())
		<div class="sidebar">
				<h4>Tags</h4>
			<div class="tags clearfix">
				@foreach($article->tags as $tag)
					<a href="{{ route('article.tag',[$tag->id]) }}" class="tag">{{$tag->name}}</a>
				@endforeach
			</div>
		</div>
	@endunless
	@include("sidebar")
	</div>
@endsection