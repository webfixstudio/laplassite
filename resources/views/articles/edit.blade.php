@extends('app')

@section("content")
<div class="row">
	<div class="col-md-8 col-sm-8">
		<h2>Edit:{!! $article->title !!}</h2>
		<hr>
		@include('errors.list')
		{!! Form::model($article, ['route' => ['article.update', $article], 'method' => 'PUT']) !!}
		<div class="form-group">
			{!! Form::label('title', 'Title:') !!}
			{!! Form::text('title', null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!!  Form::label('body', 'Body:') !!}
			{!!  Form::textarea('body', null, ['class' => 'form-control ter']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('category_list', 'Categories:') !!}
			{!! Form::select('category_list[]', $category, $article->category->pluck('id')->toArray(), ['class' => 'form-control','multiple']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('published_at', 'Published On:') !!}
			{!! Form::date('published_at', $article->published_at, ['class'=>'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('tag_list', 'Tags:') !!}
			{!! Form::select('tag_list[]', $tags, $article->tags->pluck('id')->toArray(), ['class' => 'form-control', 'multiple' => 'multiple']) !!}
		</div>
		{!! Form::submit('Update Article',['class' => 'btn btn-primary']) !!}
		{!! Form::close() !!}
	</div>
	<div class="col-md-4 col-sm-4">
			@include("sidebar")
	</div>
</div>
@endsection

@section("editor")
<script type="text/javascript" src="{{asset('/user_panel/js/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript">
	tinymce.init({
		selector: ".ter",
		plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste jbimages"],
		toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
		// ===========================================
		// SET RELATIVE_URLS to FALSE (This is required for images to display properly)
		// ===========================================
		relative_urls: false
	});
	$('select').select2({
		placeholder: 'Choose a tag',
		tags:true
	});
</script>
@endsection