<div class=" sidebar">
	{!! Form::open(['route'=>'article.search', 'method'=>'POST']) !!}
		<div class="input-group">
			{!! Form::text('search', null, ['class' => 'form-control', 'placeholder'=> trans('sidebar.search_place')]) !!}
				<span class="input-group-btn">
					{!! Form::submit(trans('sidebar.search'),['class'=>'btn btn-default', 'style'=>'padding:10px 15px;' ])!!}
				</span>
		</div>
	{!! Form::close() !!}
</div>

@widget('categories')

@widget('gallery')

@section('js111111')
	<script type="text/javascript">
		$(function () {															// Когда страница загрузится
			$('.lmenu > li > a').each(function () {		// получаем все нужные нам ссылки
				var location = window.location.href;		// получаем адрес страницы
				var link = this.href;										// получаем адрес ссылки
				if (location == link) {									// при совпадении адреса ссылки и адреса окна
					// console.log('ddd');
					$(this).addClass('active');				// добавляем класс
				}
			});
		});
	</script>
@endsection