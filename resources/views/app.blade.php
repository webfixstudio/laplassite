<!DOCTYPE html>
<html class="no-js" lang="en-US">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="Nicolay Ponomarev">
	<title>Laplas &ndash; Home</title>
	<link rel="shortcut icon" href="favicon.png">
	<link rel="stylesheet" href="{{ asset('/user_panel/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/user_panel/css/plugins/isotope.css') }}"> 
	<link rel="stylesheet" href="{{ asset('/user_panel/css/plugins/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/user_panel/css/styles.css') }}">
	<link rel="stylesheet" href="{{ asset('/user_panel/css/media.css') }}">
	<link rel="stylesheet" href="{{ asset('/user_panel/css/ncs.css') }}">
	<script src="{{ asset('/user_panel/js/modernizr.custom.js') }}"></script>
</head>
<body>
<!-- Navigation Bar -->
{!! Request::is('/')? '
	<div class="page-loader">
		<div class="content"> 
			<div class="line">
				<div class="progress"></div>
			</div>
			<div class="text">Loading...</div>
		</div>
	</div>': ''!!}
<div class="navbar two {{Request::is('/')? '' : 'floating navbar-fixed-top positive'}}" role="navigation">
	<div class="container">
		{{-- <div class="col-md-10 col-md-offset-1"> --}}
		<div class="navbar-header">
			<!-- Menu for Tablets / Phones -->
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<!-- Logo -->
			<a class="navbar-brand" href="{{ url('/') }}"
			 style="font-weight: 700; font-size: 25px; text-shadow: 2px 4px 3px rgba(0, 0, 0, 0.3);"
			 >
				<i class="fa fa-plus" style="color: red"></i> Laplas
			</a>
		</div>
		<div class="collapse navbar-collapse" id="navbar-collapse">
			<!-- Items -->
			<ul class="nav navbar-nav navbar-right">
					<li><a href="{{url('/')}}">{{trans('menu.home')}}</a></li>
					<li><a href="{{route('articles')}}">{{trans('menu.articles')}}</a></li>
					<li><a href="{{route('images')}}">{{trans('menu.images')}}</a></li>
					<li><a href="{{route('contact')}}">{{trans('menu.contact')}}</a></li>
					<li><a href="{{ url('/about') }}">{{ trans('menu.about') }}</a></li>
				<!-- Auth -->
				@if (Auth::guest())
					<li><a href="{{ url('/auth/login') }}">{{ trans('menu.login') }}</a></li>
					<li><a href="{{ url('/auth/register') }}">{{ trans('menu.register') }}</a></li>
				@else
					<li class="dropdown">
						<a>{{ Auth::user()->name }} <span class="caret"></span></a>
						<ul class="dropdown-menu top-drop">
							@if (Auth::user()->role == 1)
								<li><a href="{{route('admin')}}">{{trans('menu.admin')}}</a> </li>
							@endif
							<li><a href="{{route('user.show',[Auth::id()])}}">{{trans('menu.profile')}}</a></li>
							<li><a href="{{route('user.edit',[Auth::id()])}}">{{trans('menu.edit_profile')}}</a></li>
							<li><a href="{{ url('/auth/logout') }}">{{trans('menu.logout')}}</a></li>
						</ul>
					</li>
				@endif
			</ul>
		</div>
		{{-- </div> --}}
	</div>
</div>
<!-- Intro -->

@yield('wellcome')
<section class="{{ Request::is('/')? '' : 'offsetTop'}}">
	<div class="container">
		<div class="content">
			@yield('content')
		</div>
	</div>
</section>

<section>
	@yield('footer')
</section>

{!! Request::is('/')? '' : '
<footer class="footer offsetTopS offsetBottomS">
	<div class="container offsetTopS offsetBottomS">
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<a class="to-top"><i class="fa fa-angle-up"></i></a>
				<span>2015 &copy; Laplas. All rights reserved.</span>
			</div>
			<div class="col-md-6 col-sm-6 social">
				<a href="#" title="Facebook"><i class="fa fa-facebook"></i></a>
				<a href="#" title="Twitter"><i class="fa fa-twitter"></i></a>
				<a href="#" title="Instagram"><i class="fa fa-instagram"></i></a>
				<a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a>
			</div>
		</div>
	</div>
</footer>
'!!}
<!-- Footer -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ asset('/user_panel/js/jquery.min.js') }}"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
{{-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> --}}
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ asset('/user_panel/bootstrap/js/bootstrap.min.js') }}"></script>
{{-- <script src="{{ asset('/user_panel/bootstrap/js/carousel.js') }}"></script> --}}
<!-- Other Scripts -->
<script src="{{ asset('/user_panel/js/retina.min.js') }}"></script>
<script src="{{ asset('/user_panel/js/jquery.scrollto.min.js') }}"></script>
{{-- <script src="{{ asset('/user_panel/js/smoothscroll.js') }}"></script> --}}

{{-- <script src="{{ asset('/user_panel/js/jquery.parallax.js') }}"></script> --}}

<script src="{{ asset('/user_panel/js/jquery.nav.js') }}"></script>
{{-- <script src="{{ asset('/user_panel/js/jquery.knob.js') }}"></script> --}}
<script src="{{ asset('/user_panel/js/jquery.tweet.min.js') }}"></script>
<!-- Main Script -->
<script src="{{ asset('/user_panel/js/prodo.min.js') }}"></script>
{{-- <script src="{{ asset('/user_panel/js/theme-switcher.js') }}"></script> --}}



{{-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script> --}}

@yield('editor')
@yield('js')
<!-- yield JS -->
<script type="text/javascript">
	var Prodo = {
		loader: false,
		animations: true
	};
	$(function () {
		$(".gruf").draggable();
		$(".datepicker").datepicker();
	});
	$('.carousel').carousel({
		interval: 3000
	})    
</script>
</body>
</html>