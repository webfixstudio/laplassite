<div class="sidebar">
	<h4>{{ trans('sidebar.gallery') }}</h4>
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			@for($i=1;$i<count($images);$i++)
				<li data-target="#myCarousel" data-slide-to="{{$i}}"></li>
			@endfor
		</ol>
		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
			@foreach($images as $image)
				<div class="item">
					<img src="/{{$image->file}}" style="width: 100%">
					<div class="carousel-caption">
						<h3 style="color:#19AAD8">{{$image->caption}}</h3>
						<p>{{substr($image->description,0,50)}}</p>
					</div>
				</div>
			@endforeach
		</div>
		<!-- Left and right controls -->
		<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</div>

@section('js')
    <script>
        $(document).ready(function () {
            $('.item:first-child').addClass('active');
        });
    </script>
@endsection