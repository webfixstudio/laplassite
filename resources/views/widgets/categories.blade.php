<div class="sidebar">
	<h4>{{trans('sidebar.categories')}}</h4>
	<ul class="menu list-group">
	@foreach($categories as $category)
		<li class="list-group-item">
			<span class="badge">{{$category->articles->count()}}</span>
			<a href="{{route('article.category', $category->id)}}"> 
				{{$category->category_name}} 
			</a>
		</li>
	@endforeach
	</ul>
</div>
