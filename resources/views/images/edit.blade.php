@extends('app')

@section('content')
   <div class="col-md-10 col-md-offset-1">
      <h2>Edit Image</h2>
   @include('images.error')
   {!! Form::model($image,['route' => ['image.update',$image->id], 'method' => 'PUT', 'files'=>true]) !!}

      <img src="{{ asset($image->file) }}" height="150" />
      <div class="form-group">
         <label for="userfile">Image File</label>
         {!! Form::file('userfile',null,['class'=>'form-control']) !!}
      </div>
      <div class="form-group">
         <label for="tag">Tag</label>
         {!!Form::select('tag', ['photography'=>'Photography', 'illustration'=>'Illustration', 'branding'=>'Branding', 'personal'=>'Personal'], null,['class'=>'form-control'])!!}
      </div>
      <div class="form-group">
         <label for="caption">Caption</label>
         {!! Form::text('caption',null,['class'=>'form-control']) !!}
      </div>
      <div class="form-group">
         <label for="description">Description</label>
         {!! Form::textarea('description',null,['class'=>'form-control']) !!}
      </div>

      <button type="submit" class="btn btn-primary">Save</button>
      <a href="{{ route('images') }}" class="btn btn-warning">Cancel</a>

   {!! Form::close() !!}
</div>
@endsection