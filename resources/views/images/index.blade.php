@extends('app')

@section('content')
<div class="row">
	<div class="col-md-12 col-sm-12">
	<h1>Images</h1>
	@if(count($images) > 0)
		@if (Auth::user())
			<a style="margin-bottom: 10px" href="{{ route('image.create') }}" class="btn btn-primary" role="button">
				Add New Image
			</a>			
		@endif
			<div class="row">
			@include('images.error')
		@endif
		@forelse($images as $image)
				<div class="col-md-4">
					<div class="thumbnail">
						<a href="{{ route('image.show',$image->id) }}"><img src="{{asset($image->file)}}" class="img"/></a>
						<div class="caption">
							<h3>{{$image->caption}}</h3>
							<p>{{$image->tag}}</p>
							<p>{!! substr($image->description, 0,100) !!}</p>
							<div class="row text-center" style="padding-left:1em;">
								@if (!Auth::guest())
									<a href="{{ route('image.edit',[$image->id]) }}" class="btn btn-primary pull-left"><i
												class="fa fa-pencil-square-o"></i>Edit</a>
									<span class="pull-left">&nbsp;</span>
									{!! Form::open(['route'=>['image.destroy',$image->id], 'class'=>'pull-left']) !!}
									{!! Form::hidden('_method', 'DELETE') !!}
									{!! Form::button('<i class="fa fa-trash-o"></i> Delete', ['type' => 'submit', 'class'=> 'btn btn-danger', 'onclick'=>'return confirm("Are you sure?")' ])!!}
									{!! Form::close() !!}
								@endif
							</div>
						</div>
					</div>
				</div>
			@empty
				<p>No images yet, <a href="{{ url('/image/create') }}">add a new one</a>?</p>
			@endforelse
		</div>
{{-- </div> --}}

	<section id="portfolio" class="section offsetTop">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
						<h2>Images</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 portfolio-filters">
					<a class="active" data-filter="*" href="#">All</a>
					<a data-filter=".photography" href="#" class="">Photography</a>
					<a data-filter=".illustration" href="#" class="">Illustration</a>
					<a data-filter=".branding" href="#" class="">Branding</a>
					<a data-filter=".personal" href="#">Personal</a>
				</div>
			</div>
		</div>
		<div class="container-fluid offsetTop">
			<div data-on-line-xs="2" data-on-line-sm="4" data-on-line-md="5" data-on-line-lg="5" class="row portfolio-items clearfix isotope">
				@foreach($images as $image)
				<div class="animation-from-left">
					<div rel="item-one" class="portfolio-item {{$image->tag}} isotope-item">
						<img alt="Project" src="{{$image->file}}">
						<div class="overlay"></div>
						<div class="details">{{$image->caption}}</div>
						<a data-url="image/{{$image->id}}" href="image/{{$image->id}}"></a>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</section>
		{!! $images->render() !!}
</div>
</div>
@endsection

@section('js')
	<script src="{{ asset('/user_panel/js/jquery.isotope.min.js') }}"></script>
@endsection
