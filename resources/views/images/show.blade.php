@extends('app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <h2>Image: {{ $image->caption }}</h2>

        <div class="row addbuttonmain">
            <div class="col-md-6  ">
                <a href="{{ route('images') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>Back</a>
            </div>
            <div class="col-md-6">
                @if (!Auth::guest())
                    <a href="{{ route('image.edit', [$image->id]) }}" class="btn btn-primary pull-right"><i
                                class="fa fa-pencil-square-o"></i>Edit</a>
                    <span class="pull-right">&nbsp;</span>
                    {!! Form::open(['route'=>['image.destroy',$image->id], 'class'=>'pull-right']) !!}
                    {!! Form::hidden('_method', 'DELETE') !!}
                    {!! Form::button('<i class="fa fa-trash-o"></i> Delete', ['type' => 'submit', 'class'=> 'btn btn-danger', 'onclick'=>'return confirm("Are you sure?")' ])!!}
                    {!! Form::close() !!}
                @endif
            </div>
        </div>
        <form class="row form-horizontal">
            <img src="{{ asset($image->file) }}" width="100%"/>

            <div class="form-group">
                <label class="col-sm-2 control-label">Caption</label>

                <div class="col-sm-10">
                    <p class="form-control-static">{{ $image->caption }}</p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Description</label>

                <div class="col-sm-10">
                    <p class="form-control-static">{{ $image->description }}
                </div>
            </div>

        </form>
    </div>

@endsection
