@extends('app')

@section('content')
   <div class="col-md-10 col-md-offset-1">
      <h2>Add Image</h2>
   @include('images.error')
   {!! Form::open(['route'=>'image.store', 'method'=>'POST', 'files'=>'true']) !!}

      <div class="form-group">
         <label for="userfile">Image File</label>
         <input type="file" name="userfile">
      </div>
      <div class="form-group">
         <label for="tag">Tag</label>
         {!!Form::select('tag', ['photography'=>'Photography', 'illustration'=>'Illustration', 'branding'=>'Branding', 'personal'=>'Personal'], 'personal',['class'=>'form-control'])!!}
      </div>
      
      <div class="form-group">
         <label for="caption">Caption</label>
         <input type="text" class="form-control" name="caption" value="">
      </div>

      <div class="form-group">
         <label for="description">Description</label>
         <textarea class="form-control" name="description"></textarea>
      </div>

      <button type="submit" class="btn btn-primary">Upload</button>
      <a href="{{ route('images') }}" class="btn btn-warning">Cancel</a>
   {!! Form::close() !!}
</div>
@endsection