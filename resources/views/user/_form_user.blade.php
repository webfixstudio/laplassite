<h1>{{$user->name}}</h1>

@if($user->foto)
    <img src="/files/users_images/{{$user->foto}}" alt="{{$user->name}}" class="ncsimg"/>
@else
    <img src="/files/users_images/default.jpg" alt="{{$user->name}}" class="ncsimg"/>
    @endif
            <!-- <a href="/upload" class="btn btn-danger btn-lg" role="button">Upload</a> -->

    <table class="table">
        <tr>
            <th>Skype</th>
            <th>@if($user->skype)
                    {{$user->skype}}
                @else
                    Not set
                @endif
            </th>
        </tr>
        <tr>
            <th>Phone</th>
            <th>@if($user->phone)
                    {{$user->phone}}
                @else
                    Not set
                @endif
            </th>
        </tr>
        <tr>
            <th>E-mail</th>
            <th>@if($user->email)
                    {{$user->email}}
                @else
                    Not set
                @endif
            </th>
        </tr>
        <tr>
            <th>Facebook</th>
            <th>@if($user->facebook)
                    {{$user->facebook}}
                @else
                    Not set
                @endif
            </th>
        </tr>
        <tr>
            <th>Birthday</th>
            <th>@if($user->birthday)
                    {{ \Carbon\Carbon::parse($user->birthday)->format('d.m.Y')}}
                @else
                    Not set
                @endif
            </th>
        </tr>
    </table>
    <hr>
    <a href="/user/{{$user->id}}/edit" class="btn btn-primary btn-lg" role="button">Edit</a>