@extends('app')

@section('content')
    <div class="row">
        <div class="col-md-8 col-sm-8">
            <h1>All Users</h1>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Photo</th>
                    <th>Name</th>
                    <th>E-mail</th>
                    <th>Create</th>
                    <th>Last Visit</th>
                </tr>
                </thead>
                @foreach($users as $user)
                    <tr>
                        <th>{{$user->id}}</th>
                        <th><img src="{{'/files/users_images/'.$user->foto}}" class="img-circle" style="width: 50px"></th><th><a href="{{url('user/'.$user->id)}}">  {{$user->name}}</a></th>
                        <th>{{$user->email}}</th>
                        <th>{{ \Carbon\Carbon::parse($user->created_at)->format('d.m.Y h:m')}}</th>
                        <th>{{ \Carbon\Carbon::parse($user->updated_at)->format('d.m.Y h:m')}}</th>
                    </tr>
                @endforeach
            </table>
            {!! $users->render() !!}
        </div>
        <div class="col-md-4 col-sm-4">

                @include("sidebar")

        </div>
    </div>
@stop