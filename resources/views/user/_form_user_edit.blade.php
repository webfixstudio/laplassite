<h2>Edit:{!! $user->name !!}</h2>

<hr>
{!! Form::model($user, ['route'=>"user.update",'method'=>'POST', 'files'=>true]) !!}

<!-- Name Form Input -->
<div class="form-group">
    <img src="/files/users_images/{{$user->foto}}" alt="{{$user->name}}" class="img-ounded ncsimg"/>
    {!! Form::file('foto') !!}
</div>

<div class="form-group">
    {!! Form::label('name', 'Username:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Skype Form Input -->
<div class="form-group">
    {!! Form::label('skype', 'Skype:') !!}
    {!! Form::text('skype', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Form Input -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control', 'data-mask'=> 'phone', 'placeholder'=>'(999) 999-9999']) !!}
</div>

<!-- Facebook Form Input -->
<div class="form-group">
    {!! Form::label('facebook', 'Facebook:') !!}
    {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
</div>

<!-- Birthday Form Input -->
<div class="form-group">
    {!! Form::label('birthday', 'Birthday:') !!}
    {!! Form::text('birthday', null, ['class' => 'form-control']) !!}
</div>

    {!! Form::submit('Save',['class' => 'btn btn-primary btn-lg']) !!}

{!! Form::close() !!}

@include('errors.list')

@section('js')

<script type="text/javascript" src="{{asset('/admin_panel/js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('/admin_panel/js/jquery.maskedinput/jquery.maskedinput.js')}}" type="text/javascript"></script>
<script type="text/javascript">    /*Form Masks*/
    $(document).ready(function(){
    $("[data-mask='date']").mask("99/99/9999");
    $("[data-mask='phone']").mask("(999) 999-9999");
    $("[data-mask='phone-ext']").mask("(999) 999-9999? x99999");
    $("[data-mask='phone-int']").mask("+33 999 999 999");
    $("[data-mask='taxid']").mask("99-9999999");
    $("[data-mask='ssn']").mask("999-99-9999");
    $("[data-mask='product-key']").mask("a*-999-a999");
    $("[data-mask='percent']").mask("99%");
    $("[data-mask='currency']").mask("$999,999,999.99");
    });//End of masks
</script>
    <script type="text/javascript">
        $("[data-mask='phone']").cleanVal();
    </script>

@endsection