@extends('app')
@section('content')
    <div class="span7 offset1">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <h2>{!! Session::get('success') !!}</h2>
            </div>
        @endif
        <h2>Upload form</h2>
        {!! Form::open(array('url'=>'user/upload','method'=>'POST', 'files'=>true)) !!}
        <div class="control-group">
            <div class="controls">
                {!! Form::file('image') !!}
                {{$errors->first('image', null)}}
                @if(Session::has('error'))
                    <p class="alert alert-danger">{!! Session::get('error') !!}</p>
                @endif
            </div>
        </div>
        <div id="success"></div>
            <hr>
        {!! Form::submit('Upload', array('class'=>'btn btn-danger')) !!}
        {!! Form::close() !!}
    </div>
@stop