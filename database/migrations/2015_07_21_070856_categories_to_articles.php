<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CategoriesToArticles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('categories', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer('parent_id')->default(0);
			$table->string('category_name',255);
			$table->string('category_alias',255);
			$table->text('description');
			$table->string('category_image',255);
			$table->string('meta_keywords',255);
			$table->string('meta_description',255);
			$table->string('meta_title',255);
			$table->timestamps();
		});


		Schema::create('article_categories', function (Blueprint $table)
		{
			$table->integer('article_id')->unsigned()->index();
			$table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');

			$table->integer('categories_id')->unsigned()->index();
			$table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
			$table->timestamps();
		});

		// Schema::table('articles', function (Blueprint $table)
		// {
		// 	$table->integer('category_id')->after('user_id');
		// });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::drop('article_category');

		Schema::drop('categories');

		// Schema::table('articles', function ($table) {
		// 	$table->dropColumn('category_id');
		// });
	}

}