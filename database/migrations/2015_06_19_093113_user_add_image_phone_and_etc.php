<?php
//use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class UserAddImagePhoneAndEtc extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->date('birthday')->after('email');
            $table->string('skype')->after('email');
            $table->string('facebook')->after('email');
            $table->integer('phone')->after('email');
            $table->string('foto')->default('default.jpg')->after('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn(['foto','phone', 'facebook', 'skype', 'birthday']);

        });
    }

}
