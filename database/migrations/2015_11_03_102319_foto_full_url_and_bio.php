<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Builder;

class FotoFullUrlAndBio extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('users', function (Blueprint $table) {
            $table->text('bio')->after('birthday');
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->increments('id')->unsigned()->change();
        });

       /* Schema::table('articles', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('category_id')->unsigned()->change();
            $table->index('category_id');
            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');
        });*/

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

 /* Schema::table('articles', function (Blueprint $table) {
            $table->dropIndex('articles_category_id_index');
        });
*/
   Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('bio');

        });



    }


}
