<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('configs', function(Blueprint $table)
		{
			$table->string('path_user_images');

		});
		DB::table('configs')->insert(
			array(
				array('path_user_images' => 'admin'),

			));
		//DB::unprepared(file_get_contents('default_data/users_dump.sql'));
		//DB::unprepared(file_get_contents('default_data/articles_dump.sql'));

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('configs');
	}

}
